#!/bin/bash

emerge app-cdr/cdrtools app-portage/gentoolkit app-portage/portage-utils dev-libs/libisoburn dev-util/dialog net-misc/netifrc sys-apps/makedev sys-apps/usbutils sys-fs/squashfs-tools sys-kernel/genkernel sys-kernel/gentoo-sources sys-kernel/hardened-sources 
mkdir -p ./src
cp -R /usr/src/* src/

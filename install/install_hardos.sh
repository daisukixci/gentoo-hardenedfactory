#!/bin/bash

# @Purpose: Deploy the hardened OS into a system
#
# @Author: PIQUENOT Gaetan
# @Property: AIRBUS DEFENSE AND SPACE 
# @Licence:
# @Copyright:
source /lib/rc/sh/functions.sh
test -f "./conf/hardened_factory.cfg" || eend "Factory configuration file is missing" || exit 1
test -f "./conf/services.cfg" || eend "Services configuration file is missing" || exit 1
source ./conf/hardened_factory.cfg
source ./conf/services.cfg
source ./services.sh

HARD_NAME="hardened-kernel-airbus"
TARGET_DIR="/root/target"
INSTALL_DIR=`pwd`
INSTALL_CFG_DIR="${INSTALL_DIR}/etc"
INSTALL_POR_DIR="${INSTALL_CFG_DIR}/portage"
INSTALL_PKG_DIR="${INSTALL_DIR}/../pkg"

GROUP_LIST=(
addsi
adsys
adnet
)

STICKY_LIST=( 
/bin/mount
/bin/passwd
/bin/su
/bin/ping
/bin/ping6
/bin/arping
/bin/umount
/usr/bin/chfn
/usr/bin/chage
/usr/bin/chsh
/usr/bin/clockdiff
/usr/bin/gpasswd
/usr/bin/expiry
/usr/bin/newgrp
)

detect_characteristics() {
    # Detection of hardware characteristics (arch, disk size, ...)
    ebegin "Detect hardware characteristics"
    test ${ARCH} = `uname -m` \
        || eend "OS architecture and hardware are different" \
        || exit -1
    VERSION=`uname -r | cut -d "-" -f1`
    # Total disk size in bytes
    SIZE_DISK=`fdisk -l | grep /dev/sda | cut -d " " -f3`
    # Ram size in bytes
    i="2"
    while [ -z "${SIZE_RAM}" ]; do
        SIZE_RAM=`free -m | sed -n 2p | cut -d " " -f${i}`
        let "i++"
    done
    SIZE_SWAP="${SIZE_RAM}M"
    AVAILABLE_INTERFACES=`ifconfig -a | grep '^e' | cut -d: -f1 `
    let "CPU_CORES = $(nproc) + 1"
    sed -i -e "s/MAKEOPTS=\"-j[1-9]\"/MAKEOPTS=\"-j${CPU_CORES}\"/g" ${INSTALL_POR_DIR}/${ARCH}/make.conf
    eend 0
}

create_parts() {
    # Create two partition one for boot
    # and one for the lvm container
    ebegin "Partitions creation"
    parted -s /dev/sda mklabel msdos
    parted -s /dev/sda mkpart primary ext2 2048s 500 || exit 1
    parted -s /dev/sda set 1 boot on || exit 1
    parted -s /dev/sda mkpart primary 500 100% || exit 1
    eend 0
}

create_lvm() {
    # Create all lvm volumes and cipher container if 
    # the option CIPHER is set to yes
    #   Create the algorithm option
    #   according to the configuration file
    #   Generate entropy for a higher security
    #   Create the Luks container and open it
    # Create the LVM container
    # Create all logical volumes
    # Create all volumes filesystem
    ebegin "Logical volumes creation"
    mkfs.ext2 /dev/sda1 > /dev/null 2>&1
    if [ "${CIPHER}" != "no" ]; then
        case ${CIPHER} in
            aes)
                CIPHER_ALGO="aes-cbc-essiv:sha256"
                ;;
            blowfish)
                CIPHER_ALGO="blowfish-cbc-plain:sha256"
                ;;
            serpent)
                CIPHER_ALGO="serpent-cbc-essiv:sha256"
                ;;
            twofish)
                CIPHER_ALGO="twofish-xts-essiv:sha256"
                ;;
            *)
                eend "Algorithm is not available"
                exit -1
                ;;
        esac
        ebegin "Entropy generation"
        #dd if=/dev/urandom of=/dev/sda2
        eend 0

        ewarn "You will be asked to put a passphrase 3 times, 2 times for set it and one time to use it"
        ewarn "You could use this following passphrase which have been generated randomly"
        dd if=/dev/urandom bs=1 count=32 2>/dev/null | base64 -w 0 | rev | cut -b 2- | rev
        cryptsetup -c ${CIPHER_ALGO} -s ${KEY_SIZE} luksFormat /dev/sda2 || eend "Impossible to create LUKS container" || exit 1
        cryptsetup luksOpen /dev/sda2 luksc

        pvcreate /dev/mapper/luksc
        vgcreate vg /dev/mapper/luksc
    else
        pvcreate /dev/sda2
        vgcreate vg /dev/sda2
    fi

    lvcreate -L ${SIZE_ROOT} -n root vg
    lvcreate -L ${SIZE_SWAP} -n swap vg
    lvcreate -L ${SIZE_ETC} -n etc vg
    lvcreate -L ${SIZE_USR} -n usr vg
    lvcreate -L ${SIZE_TMP} -n tmp vg
    lvcreate -L ${SIZE_DEV} -n dev vg
    lvcreate -L ${SIZE_VAR} -n var vg
    eend 0

    ebegin "Formating partitions"
    mkswap -L swap /dev/mapper/vg-swap >/dev/null 2>&1
    mkfs.ext4 /dev/mapper/vg-root >/dev/null 2>&1
    mkfs.ext4 /dev/mapper/vg-etc >/dev/null 2>&1
    mkfs.ext4 /dev/mapper/vg-usr >/dev/null 2>&1
    mkfs.ext4 /dev/mapper/vg-tmp >/dev/null 2>&1
    mkfs.ext4 /dev/mapper/vg-dev >/dev/null 2>&1
    mkfs.ext4 /dev/mapper/vg-var >/dev/null 2>&1
    eend 0
}

mount_parts() {
    # Mount partitions and open LUKS container if it's needed
    ebegin "Mount partitions"
    mkdir -p ${TARGET_DIR}
    mount /dev/mapper/vg-root ${TARGET_DIR}

    mkdir -p ${TARGET_DIR}/{boot,bin,sbin,etc,usr,var/log,tmp,dev,sys,proc,}
    chmod 1777 ${TARGET_DIR}/tmp
    mount /dev/sda1 ${TARGET_DIR}/boot
    swapon /dev/mapper/vg-swap
    mount /dev/mapper/vg-etc ${TARGET_DIR}/etc
    mount /dev/mapper/vg-usr ${TARGET_DIR}/usr
    mount /dev/mapper/vg-dev ${TARGET_DIR}/dev
    mount /dev/mapper/vg-var ${TARGET_DIR}/var
    mount -t tmpfs /dev/mapper/vg-tmp ${TARGET_DIR}/tmp
    mount -t proc none ${TARGET_DIR}/proc
    mount --bind /sys ${TARGET_DIR}/sys
    mount --rbind /dev ${TARGET_DIR}/dev
    mount --make-rslave ${TARGET_DIR}/dev
    eend 0
}

umount_parts() {
    # Correctly unmount partitions 
    # and LUKS container if needed
    ebegin "Unmount partitions"
    swapoff /dev/mapper/vg-swap
    umount ${TARGET_DIR}/proc
    umount ${TARGET_DIR}/sys
    umount ${TARGET_DIR}/etc
    umount ${TARGET_DIR}/usr
    umount ${TARGET_DIR}/var
    umount ${TARGET_DIR}/tmp
    umount ${TARGET_DIR}/boot
    umount ${TARGET_DIR}/dev
    umount ${TARGET_DIR}

    dmsetup remove vg-swap
    dmsetup remove vg-etc
    dmsetup remove vg-usr
    dmsetup remove vg-var
    dmsetup remove vg-dev
    dmsetup remove vg-tmp
    dmsetup remove vg-root
    if [ "${CIPHER}" == "yes" ]; then
        cryptsetup luksClose luksc
    fi
    eend 0
}

install_kernel() {
    # Kernel installation
    ebegin "Installation of kernel"
    cp -R ${INSTALL_DIR}/kernel/* ${TARGET_DIR}/boot
    cd ${TARGET_DIR}/boot
    tar -xjf ${HARD_NAME}.tar.bz2
    mv initramfs-* initramfs.img
    rm ${HARD_NAME}.tar.bz2
    cd ${INSTALL_DIR}
    eend 0
}

create_device() {
    # Create needed devices for 
    # target system
    ebegin "Device creation"
    cd ${TARGET_DIR}/dev
    MAKEDEV -d . console bus core cpu dri fd null stderr stdout stdin urandom random zero log
    MAKEDEV -xd . sda sda1 sda2 
    MAKEDEV -xd . tty tty1 tty2 tty3 tty4 tty5 tty6 tty7 tty8 tty9 
    MAKEDEV -xd . loop0 loop1 loop2 loop3 loop4 loop5 loop6 loop7 loop8 loop9 
    cd ${INSTALL_DIR}
    eend 0
}

configure_portage() {
    # Portage configuration for futur installation
    # with already prepared portage files
    # and environement variables to define 
    # binary locations
    ebegin "Configure Portage"
    mkdir -p ${TARGET_DIR}/var/tmp
    export PORTAGE_TMPDIR=${TARGET_DIR}/var/tmp
    export PKGDIR=${INSTALL_PKG_DIR}

    rsync -a /etc/portage ${TARGET_DIR}/etc/
    rsync -a /usr/portage ${TARGET_DIR}/usr/

    cp ${INSTALL_POR_DIR}/${ARCH}/make.conf ${TARGET_DIR}/etc/portage/
    cp ${INSTALL_POR_DIR}/${ARCH}/make.profile/package.* ${TARGET_DIR}/etc/portage/make.profile/
    cp ${INSTALL_POR_DIR}/${ARCH}/package.use/*.use ${TARGET_DIR}/etc/portage/package.use/

    mkdir -p ${TARGET_DIR}/lib64 
    cd ${TARGET_DIR}
    ln -s lib64 lib
    cd ${TARGET_DIR}

    eend 0
}

install_base() {
    # Install packages into target
    # from packages list
    ebegin "Install basic packages and utilities"
    export PORTAGE_TMPDIR=${TARGET_DIR}/var/tmp
    export PKGDIR=${INSTALL_PKG_DIR}

    ROOT=${TARGET_DIR} eselect news read all 1>/dev/null 
    for i in ${BASE_PKG_LIST[@]}; do
        PKG_INLINE+="${i} "
    done
    ROOT=${TARGET_DIR} PORTAGE_CONFIGROOT=${TARGET_DIR} emerge -qK ${PKG_INLINE} || eend "Impossible to install base packages" || exit 1
    eend 0
}

split_etc() {
    # Copy init.d and runlevels directly into the
    # root to allow a boot even without etc mounted
    ebegin "Split etc directory"
    cp -R ${TARGET_DIR}/etc/init.d ${TARGET_DIR}/tmp
    cp -R ${TARGET_DIR}/etc/runlevels ${TARGET_DIR}/tmp

    umount ${TARGET_DIR}/etc
    mv ${TARGET_DIR}/tmp/init.d ${TARGET_DIR}/etc/init.d 
    mv ${TARGET_DIR}/tmp/runlevels ${TARGET_DIR}/etc/runlevels 
    mount /dev/mapper/vg-etc ${TARGET_DIR}/etc

    cp ${INSTALL_CFG_DIR}/part/remount.start ${TARGET_DIR}/etc/local.d/
    eend 0
}

configure_grub() {
    # Create the correct mounted environement
    # for grub installation and configuration
    # Installation of grub and configuration
    # with change detection name script of initrd file to detect 
    # the initramfs into the boot directory
    # Besides add support for LVM and LUKS if it's needed
    # Go back to the installation environnement
    # Add some settings for LVM and LUKS 
    # support into grub.cfg if it's needed
    # Install fstab file and crypttab if 
    # it's needed
    ebegin "Configure GRUB installation environement"
    umount ${TARGET_DIR}/boot
    mount --rbind /dev ${TARGET_DIR}/dev
    mount --make-rslave ${TARGET_DIR}/dev
    chroot ${TARGET_DIR} /bin/bash -c "mount /dev/sda1 /boot"
    eend 0

    ebegin "Installation and configuration of grub"
    chroot ${TARGET_DIR} /bin/bash -c "grub-install --target=i386-pc /dev/sda" > /dev/null 2>&1
    sed -i -e "s/initramfs-\${version}.img/initramfs.img/" ${TARGET_DIR}/etc/grub.d/10_linux
    echo "GRUB_PRELOAD_MODULES=lvm" >> ${TARGET_DIR}/etc/default/grub

    if [ "${CIPHER}" != "no" ]; then
        echo "GRUB_ENABLE_CRYPTODISK=y" \
            >> ${TARGET_DIR}/etc/default/grub

        i="10"
        while [ -z ${ENCRYPT_UUID} ]; do
            ENCRYPT_UUID=`lsblk -f | sed -n 4p | cut -d " " -f${i}`
            let "i++"
        done

        sed -i -e "s/#\(GRUB_CMDLINE_LINUX=\)\"\"/\1\"crypt_root=UUID=${ENCRYPT_UUID} dolvm\"/" ${TARGET_DIR}/etc/default/grub
    else
        sed -i -e "s/#\(GRUB_CMDLINE_LINUX=\)\"\"/\1\"dolvm\"/" ${TARGET_DIR}/etc/default/grub
    fi
    chroot ${TARGET_DIR} /bin/bash -c "grub-mkconfig -o /boot/grub/grub.cfg" > /dev/null 2>&1
    eend 0

    ebegin "Return to installation environnement"
    chroot ${TARGET_DIR} /bin/bash -c "umount /boot"
    mount /dev/sda1 ${TARGET_DIR}/boot
    eend 0

    ebegin "Configuration of grub.cfg"
    sed -i -e "s/\tcryptomount -u/\tcryptomount hd0,2/g" ${TARGET_DIR}/boot/grub/grub.cfg
    sed -i -e "s/Gentoo GNU\/Linux/Airbus Defense and Space Hardened OS/g" ${TARGET_DIR}/boot/grub/grub.cfg
    eend 0

    ebegin "Fstab installation"
    if [ "${CIPHER}" == "yes" ]; then
        cp ${INSTALL_CFG_DIR}/part/crypttab ${TARGET_DIR}/etc/
    fi
    cp ${INSTALL_CFG_DIR}/part/fstab ${TARGET_DIR}/etc/
    eend 0
}

configure_initramfs() {
    # For a correct boot process
    # initramfs need fstab and iniramfs.mounts 
    # which precise parts needed to be mount
    # before etc have been mounted
    # so copy them into root
    ebegin "Initramfs configuration"
    chmod 644 ${TARGET_DIR}/etc/fstab
    umount ${TARGET_DIR}/etc
    cp ${INSTALL_CFG_DIR}/part/initramfs.mounts ${TARGET_DIR}/etc/initramfs.mounts
    cp ${INSTALL_CFG_DIR}/part/fstab.min ${TARGET_DIR}/etc/fstab
    mount /dev/mapper/vg-etc ${TARGET_DIR}/etc
    eend 0
}


configure_system() {
    # Create some directories plus home and root
    # folder into var to avoid other partitions
    # creating symbolic link
    # Locales and keyboard  configuration
    # Rc configuration
    # Misc configuration
    # for hostname, font, banner, ...
    ebegin "Folder configuration"
    mkdir -p ${TARGET_DIR}/{sys,mnt/cdrom,mnt/usb,opt,}
    mkdir -p ${TARGET_DIR}/var/{home,root,}
    ln -sf /var/home ${TARGET_DIR}/home
    ln -sf /var/root ${TARGET_DIR}/root
    eend 0

    ebegin "Locales configuration"
    sed -i -e "s/^#en_US/en_US/" ${TARGET_DIR}/etc/locale.gen
    sed -i -e "s/^#fr_FR/fr_FR/" ${TARGET_DIR}/etc/locale.gen
    chroot ${TARGET_DIR} /bin/bash -c "locale-gen > /dev/null"
    echo "LANG=\"fr_FR.UTF-8\"" >> ${TARGET_DIR}/etc/env.d/02locale
    echo Europe/Paris > ${TARGET_DIR}/etc/timezone
    ln -sf /usr/share/zoneinfo/Europe/Paris ${TARGET_DIR}/etc/localtime
    echo "keymap=\"fr\"" > ${TARGET_DIR}/etc/conf.d/keymaps
    eend 0

    ebegin "RC configuration"
    sed -i -e "s/^#rc_hotplug=\"\*\"/rc_hotplug=\"\!\*\"/" ${TARGET_DIR}/etc/rc.conf
    sed -i -e "s/^#rc_interactive=\"YES\"/rc_interactive=\"NO\"/" ${TARGET_DIR}/etc/rc.conf
    sed -i -e "s/^#rc_depend_strict=\"YES\"/rc_depend_strict=\"NO\"/" ${TARGET_DIR}/etc/rc.conf
    sed -i -e "s/^#rc_sys=\"\"/rc_sys=\"\"/" ${TARGET_DIR}/etc/rc.conf
    eend 0

    ebegin "Misc configuration"
    sed -i -e "/^consolefont=/s:default8x16:lat9w-16:" ${TARGET_DIR}/etc/conf.d/consolefont
    while [ -z ${HOSTNAME} ]; do
        ewarn "Choose your hostname"
        read HOSTNAME
    done
    echo "hostname=\"${HOSTNAME}\"" > ${TARGET_DIR}/etc/conf.d/hostname
    cp -farp ${INSTALL_CFG_DIR}/misc/issue ${TARGET_DIR}/etc/issue
    chmod 644 ${TARGET_DIR}/etc/issue
    eend 0

    # TODO
    # DEBUG ONLY
    sed -i "s/agetty 38400 tty1/agetty --noclear 38400 tty1/" ${TARGET_DIR}/etc/inittab
}

configure_security() {
    # Harden kernel configuration 
    # about network and other stuff
    # Define logout time
    # Change default file creation mask
    # Define root password
    # Clean the screen after a logout operation
    # to avoid people to read what have been doing
    ebegin "Sysctl security configuration"
    cp ${INSTALL_CFG_DIR}/sysctl/sysctl.conf ${TARGET_DIR}/etc/
    eend 0

    ebegin "Automatic logout configuration"
    echo "export TMOUT='900'" >> ${TARGET_DIR}/etc/profile
    eend 0

    ebegin "File rights restriction"
    sed -i -e "s/umask 022/umask 077/g" ${TARGET_DIR}/etc/profile
    sed -i -e "s/UMASK\t\t022/UMASK\t\t077/g" ${TARGET_DIR}/etc/login.defs
    chroot ${TARGET_DIR} /bin/bash -c "create-cracklib-dict /usr/share/dict/* >/dev/null"
    eend 0

    ewarn "You will be asked to put a ROOT password"
    chroot ${TARGET_DIR} /bin/bash -c "passwd"

    ebegin "Clean screen after logout"
    sed -i -e "s/^#clear/clear/g" ${TARGET_DIR}/etc/bash/bash_logout
    eend 0
}

configure_network() {
    # Add kernel parameters in function
    # of ipv4 and ipv6 support
    # Default network configuration for
    # IP address 
    # DNS
    # Proxy
    ebegin "Sysctl configuration"
    cat ${INSTALL_CFG_DIR}/sysctl/ipv4.conf >> ${TARGET_DIR}/etc/sysctl.conf
    if [ "${IPV6}" = on ]; then
        cat ${INSTALL_CFG_DIR}/sysctl/ipv6.conf >> ${TARGET_DIR}/etc/sysctl.conf
    else
        echo "net.ipv6.conf.all.disable_ipv6 = 1" >> ${TARGET_DIR}/etc/sysctl.conf
        echo "net.ipv6.conf.default.disable_ipv6 = 1" >> ${TARGET_DIR}/etc/sysctl.conf
        echo "net.ipv6.conf.lo.disable_ipv6 = 1" >> ${TARGET_DIR}/etc/sysctl.conf
    fi
    eend 0

    ebegin "Network configuration"
    echo 127.0.0.1       locahost.localdomain localhost > ${TARGET_DIR}/etc/hosts
    for i in ${AVAILABLE_INTERFACES}; do
        choose_ip ${i}
        choose_netmask ${i}
        echo "config_${i}=\"${IP_ADDR}/${NETMASK}\"" >> ${TARGET_DIR}/etc/conf.d/net
        ln -sf net.lo ${TARGET_DIR}/etc/init.d/net.${i}
        chroot ${TARGET_DIR} /bin/bash -c "rc-update add net.${i} boot"
    done

    ewarn "How many DNS server you will use"
    read DNS_NUMBER
    touch ${TARGET_DIR}/etc/resolv.conf
    if [ ${DNS_NUMBER} -gt 0 ] && [ ${DNS_NUMBER} -lt 3 ]; then
        i=1
        while [ ${i} -le ${DNS_NUMBER} ]; do
            ewarn "Enter the IP address of DNS server number ${i}"
            read DNS_IP
            echo "nameserver ${DNS_IP}" >> ${TARGET_DIR}/etc/resolv.conf
            let "i++"
        done
    fi
    einfo "Network configuration" && eend 0
}

configure_capabilities() {
    # Remove sticky bit from the binary list 
    # predefined by test
    ebegin "Remove sticky bit"
    for i in ${STICKY_LIST[@]}; do
        chmod u-s ${TARGET_DIR}/${i}
    done
    eend 0 

    # Provide necessary capabilities to
    # to binary list
    ebegin "Provide right capabilities"
    chroot ${TARGET_DIR} /bin/bash -c "setcap cap_sys_admin=ep /bin/mount"
    chroot ${TARGET_DIR} /bin/bash -c "setcap cap_chown,cap_dac_read_search,cap_fowner,cap_setuid=ep /bin/passwd"
    chroot ${TARGET_DIR} /bin/bash -c "setcap cap_setuid,cap_setgid=ep /bin/su"
    chroot ${TARGET_DIR} /bin/bash -c "setcap cap_net_raw=ep /bin/ping"
    chroot ${TARGET_DIR} /bin/bash -c "setcap cap_net_raw=ep /bin/ping6"
    chroot ${TARGET_DIR} /bin/bash -c "setcap cap_net_raw=ep /bin/arping"
    chroot ${TARGET_DIR} /bin/bash -c "setcap cap_sys_admin=ep /bin/umount"
    chroot ${TARGET_DIR} /bin/bash -c "setcap cap_chown,cap_setuid=ep /usr/bin/chfn"
    chroot ${TARGET_DIR} /bin/bash -c "setcap cap_dac_read_search=ep /usr/bin/chage"
    chroot ${TARGET_DIR} /bin/bash -c "setcap cap_chown,cap_setuid=ep /usr/bin/chsh"
    chroot ${TARGET_DIR} /bin/bash -c "setcap cap_net_raw=ep /usr/bin/clockdiff"
    chroot ${TARGET_DIR} /bin/bash -c "setcap cap_chown,cap_dac_override,cap_setuid=ep /usr/bin/gpasswd"
    chroot ${TARGET_DIR} /bin/bash -c "setcap cap_dac_override,cap_setgid=ep /usr/bin/expiry"
    chroot ${TARGET_DIR} /bin/bash -c "setcap cap_dac_override,cap_setgid=ep /usr/bin/newgrp"
    #chroot ${TARGET_DIR} /bin/bash -c "setcap TODO= /usr/bin/sudo"
    #chroot ${TARGET_DIR} /bin/bash -c "setcap TODO= /usr/bin/newuidmap"
    #chroot ${TARGET_DIR} /bin/bash -c "setcap TODO= /usr/bin/newgidmap"
    #chroot ${TARGET_DIR} /bin/bash -c "setcap TODO= /usr/bin/mailq"
    #chroot ${TARGET_DIR} /bin/bash -c "setcap TODO= /usr/bin/nullmailer-queue"
    #chroot ${TARGET_DIR} /bin/bash -c "setcap TODO=ep /usr/lib64/misc/ssh-keysign"
    #chroot ${TARGET_DIR} /bin/bash -c "setcap TODO=ep /usr/libexec/lxc/lxc-user-nic"
    eend 0
}

configure_pam() {
    # Configure PAM authentification modules to restrict
    # Ressources per group into limits.conf
    # Console just for admin and rsi into access.conf from local and local network
    # Define a password policy to avoid weak passwords
    # Copy all pam.d configuration file already configured
    ebegin "Configure PAM"
    echo '*         soft    core        0' > ${TARGET_DIR}/etc/security/limits.conf
    echo '*         hard    core        0' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '*         hard    nproc       15' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '*         hard    rss         10000' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '*         -       maxlogins   2' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '@addsi    hard    core        10000' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '@addsi    soft    nproc       20' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '@addsi    hard    nproc       35' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '@addsi    -       maxlogins   1' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '@adsys    hard    core        10000' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '@adsys    soft    nproc       20' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '@adsys    hard    nproc       35' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '@adsys    -       maxlogins   1' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '@adnet    hard    core        10000' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '@adnet    soft    nproc       20' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '@adnet    hard    nproc       35' >> ${TARGET_DIR}/etc/security/limits.conf
    echo '@adnet    -       maxlogins   1' >> ${TARGET_DIR}/etc/security/limits.conf

    ewarn "Choose your remote ip address authorized to access console from network"
    ewarn "You can choose a single host ip address or a network ip address with it netmask"
    ewarn "Sample: 192.168.1.28 or 192.168.1.0/24"
    read REMOTE_IP
    echo "-:ALL EXCEPT wheel addsi adsys adnet:LOCAL ${REMOTE_IP}" > ${TARGET_DIR}/etc/security/access.conf
    sed -i -e "s/LOG_OK_LOGINS\tno/LOG_OK_LOGINS\tyes/" ${TARGET_DIR}/etc/login.defs
    sed -i -e "s/\(PASS_MAX_DAYS\)\t99999/\1\t60/g" ${TARGET_DIR}/etc/login.defs
    sed -i -e "s/^#SULOG_FILE/SULOG_FILE/g" ${TARGET_DIR}/etc/login.defs
    sed -i -e "s/DEFAULT_HOME\tyes/DEFAULT_HOME\tno/" ${TARGET_DIR}/etc/login.defs
    cp ${INSTALL_CFG_DIR}/pam/* ${TARGET_DIR}/etc/pam.d/
    eend 0
}

create_users_groups() {
    # Create administration groups and default users
    ebegin "Permissions configuration"
    cp ${INSTALL_CFG_DIR}/user/sudoers ${TARGET_DIR}/etc
    cp ${INSTALL_CFG_DIR}/user/admin ${TARGET_DIR}/etc/sudoers.d/
    sed -i -e "s/HOSTNAME/${HOSTNAME}/g" ${TARGET_DIR}/etc/sudoers.d/admin
    eend 0

    ebegin "Users and groups creation"
    for group in ${GROUP_LIST[@]}; do
        chroot ${TARGET_DIR} /bin/bash -c "groupadd ${group}"
        ewarn "Choose your ${group} user"
        read USER
        chroot ${TARGET_DIR} /bin/bash \
            -c "useradd -m ${USER} -g ${group}"
        ewarn "Choose ${USER} password"
        chroot ${TARGET_DIR} /bin/bash \
            -c "passwd ${USER}"
    done
    eend 0
}

configure_host_syslog() {
    # Configure host syslog
    ebegin "Configure syslog"
    cp ${INSTALL_CFG_DIR}/log/syslog-ng.conf ${TARGET_DIR}/etc/syslog-ng/
    chroot ${TARGET_DIR} /bin/bash -c "rc-update add syslog-ng default"
    eend 0
}

configure_lxc() {
    # Create the host brige configuration for
    # network support into container
    # Put bridge interface to startup process
    # Copy default guest configuration file on the target OS
    ebegin "LXC host configuration"
    mkdir -p ${TARGET_DIR}/srv/lxc
    echo "vlans_eth0=\"1\"" >> ${TARGET_DIR}/etc/conf.d/net
    echo "config_eth0_1=\"null\"" >> ${TARGET_DIR}/etc/conf.d/net
    echo "bridge_add_eth0_1=\"br0.1\"" >> ${TARGET_DIR}/etc/conf.d/net
    echo "config_br0_1=\"${1}/${2}\"" >> ${TARGET_DIR}/etc/conf.d/net
    echo "brctl_br0_1=\"setfd 0\n\tsethello 10\n\tstp off\"" >> ${TARGET_DIR}/etc/conf.d/net

    cd ${TARGET_DIR}/etc/init.d/
    ln -s net.lo net.br0.1
    cd ${INSTALL_DIR}
    chroot ${TARGET_DIR} /bin/bash -c "rc-update add net.br0.1 boot"
    eend 0

    ebegin "LXC default guest configuration"
    cp ${INSTALL_CFG_DIR}/lxc/lxc_guest.conf ${TARGET_DIR}/etc/lxc/guest.conf
    sed -i -e "s/bridge_ip/${BRIDGE_IP}/" ${TARGET_DIR}/etc/lxc/guest.conf
    eend 0

    ## TODO
    #echo "In works"
    #sed -i -e "s/#rc_controller_cgroups/rc_controller_cgroups/" ${TARGET_DIR}/etc/rc.conf
    #ebegin "Creation of lxc unprivileged user"
    #touch ${TARGET_DIR}/etc/subuid ${TARGET_DIR}/etc/subgid
    ##echo "lxc.id_map = u 0 100000 65536" >> ${TARGET_DIR}/etc/lxc/guest.conf
    ##echo "lxc.id_map = g 0 100000 65536" >> ${TARGET_DIR}/etc/lxc/guest.conf
    ##chroot ${TARGET_DIR}/home/lxc-user su - lxc-user -c "cmd"
    #eend 0
}

install_editor() {
    # Install selected editor and put a default configuration
    export PORTAGE_TMPDIR=${TARGET_DIR}/var/tmp
    export PKGDIR=${INSTALL_PKG_DIR}
    ROOT=${TARGET_DIR} eselect news read all 1>/dev/null 
    if [ "${EMACS}" = "on" ]; then
        einfo "Emacs installation and configuration"
        echo "export EDITOR='/usr/bin/emacs'" >> ${TARGET_DIR}/etc/profile
        for i in ${EMACS_PKG[@]}; do
            PKG_INLINE="${i} "
        done
        ROOT=${TARGET_DIR} PORTAGE_CONFIGROOT=${TARGET_DIR} emerge -qK ${PKG_INLINE} || eend "Impossible to install packages" || exit 1
        cp ${INSTALL_CFG_DIR}/misc/emacs ${TARGET_DIR}/etc/emacs/
        eend 0
    fi
    if [ "${NANO}" = "on" ]; then
        einfo "Nano installation and configuration"
        echo "export EDITOR='/usr/bin/nano'" >> ${TARGET_DIR}/etc/profile
        for i in ${NANO_PKG[@]}; do
            PKG_INLINE="${i} "
        done
        ROOT=${TARGET_DIR} PORTAGE_CONFIGROOT=${TARGET_DIR} emerge -qK ${PKG_INLINE} || eend "Impossible to install packages" || exit 1
        cp ${INSTALL_CFG_DIR}/misc/nanorc ${TARGET_DIR}/etc/
        cp -R ${INSTALL_CFG_DIR}/misc/nano ${TARGET_DIR}/usr/share/
        eend 0
    fi
    if [ "${VIM}" = "on" ]; then
        einfo "Vim installation and configuration"
        echo "export EDITOR='/usr/bin/vim'" >> ${TARGET_DIR}/etc/profile
        for i in ${VIM_PKG[@]}; do
            PKG_INLINE="${i} "
        done
        ROOT=${TARGET_DIR} PORTAGE_CONFIGROOT=${TARGET_DIR} emerge -qK ${PKG_INLINE} || eend "Impossible to install packages" || exit 1
        cp ${INSTALL_CFG_DIR}/misc/vimrc ${TARGET_DIR}/etc/vim/
        eend 0
    fi
}

choose_ip() {
    # A function to get ip address of all it requires
    # verifying input is really IPV4 or IPV6 address
    IP_REGEX="^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))|((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|(([0-9A-Fa-f]{1,4}:){0,5}:((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|(::([0-9A-Fa-f]{1,4}:){0,5}((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|([0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})|(::([0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:))$"

    while true; do
        ewarn "Choose your $(echo ${1} | tr '[:upper:]' '[:lower:]') IP address"
        read IP_ADDR
        if [[ ${IP_ADDR} =~ ${IP_REGEX} ]]; then
            if [ "${1}" = "BRIDGE" ]; then
                BRIDGE_IP="${IP_ADDR}"
            fi
            break
        fi
        eerror "Invalid IP address"
    done
}

choose_netmask() {
    # Get the netmask into CIDR notation and 
    # verifying it a number < 100
    NETMASK_RULE="[1-9][1-9]?"
    while true; do
        ewarn "Choose your $(echo ${1} | tr '[:upper:]' '[:lower:]') netmask in CIDR notation (without /)"
        read NETMASK
        if [ "${NETMASK}" != "${NETMASK_RULE}" ]; then
            break
        fi
        eerror "Invalid netmask"
    done
}

install_pkg_list(){
    # INstall a packages list from name of service
    export PORTAGE_TMPDIR=${TARGET_DIR}/var/tmp
    export PKGDIR=${INSTALL_PKG_DIR}
    ROOT=${TARGET_DIR} eselect news read all 1>/dev/null 

    PKG_LIST_NAME="${1}_PKG"
    eval PKG_LIST=\${$PKG_LIST_NAME[@]}
    PKG_INLINE=""
    for i in ${PKG_LIST[@]}; do
        PKG_INLINE+="${i} "
    done

    ROOT=${TARGET_DIR} PORTAGE_CONFIGROOT=${TARGET_DIR} emerge -qK ${PKG_INLINE} \
        || eend "Impossible to install ${1} packages" \
        || exit 1
}

set_acl() {
    ebegin "Set ACL rules"
    chroot ${TARGET_DIR} /bin/bash \
        -c "setfacl -R -m g::rwx /var/log/"
    chroot ${TARGET_DIR} /bin/bash \
        -c "setfacl -R -m g::rwx /etc/"

    chroot ${TARGET_DIR} /bin/bash \
    	-c "setfacl -R -m g:addsi:rwx /var/log/"
    chroot ${TARGET_DIR} /bin/bash \
    	-c "setfacl -R -m g:addsi:rwx /etc/"

    chroot ${TARGET_DIR} /bin/bash \
    	-c "setfacl -m g:adsys:rwx /var/log/daemon.log"
    chroot ${TARGET_DIR} /bin/bash \
    	-c "setfacl -m g:adsys:rwx /var/log/dmesg"
    chroot ${TARGET_DIR} /bin/bash \
    	-c "setfacl -m g:adsys:rwx /var/log/kern.log"
    chroot ${TARGET_DIR} /bin/bash \
    	-c "setfacl -m g:adsys:rwx /var/log/messages"
    chroot ${TARGET_DIR} /bin/bash \
    	-c "setfacl -m g:adsys:rwx /var/log/syslog"
    chroot ${TARGET_DIR} /bin/bash \
    	-c "setfacl -R -m g:adsys:rwx /var/log/lxc/"
    chroot ${TARGET_DIR} /bin/bash \
    	-c "setfacl -R -m g:adsys:rwx /etc/lxc/"

    chroot ${TARGET_DIR} /bin/bash \
    	-c "setfacl -m g:adnet:rwx /etc/conf.d/net"
    chroot ${TARGET_DIR} /bin/bash \
    	-c "setfacl -m g:adnet:rwx /etc/networks"
    eend 0
}

configure_firewall() {
    # Firewall nstallation and configuration 
    # with predefined rules
    ebegin "Install firewall"
    install_pkg_list FIREWALL
    eend 0

    ebegin "Firewall configuration"
    mkdir -p ${TARGET_DIR}/var/res
    cp ${INSTALL_CFG_DIR}/net/firewall-rules.sh ${TARGET_DIR}/var/res
    eend 0
}

disable_root() {
    # Disable root privileges dropping 
    # his capabilities at login and forbidden 
    # root login
    ebegin "Disable root account"
    cp ${INSTALL_CFG_DIR}/cap/capabilities.conf ${TARGET_DIR}/etc/security/
    sed -i -e '1d' ${TARGET_DIR}/etc/sudoers
    sed -i -e "s/wheel //g" ${TARGET_DIR}/etc/security/access.conf
    eend 0
}

configure_pax() {
    # Install and configure PaX and GrSec
    ebegin "Install PaX control"
    install_pkg_list PAXCTL
    eend 0

    ebegin "Configure PaX and GrSec control"
    cp ${INSTALL_CFG_DIR}/sysctl/05-grsecurity.conf ${TARGET_DIR}/etc/sysctl.d/
    eend 0
}


protect_log() {
    # Put logs immutable 
    ebegin "Protect log with immutability"
    sed -i -e "s/LOG_IMMUTABILTY=\".*\"/LOG_IMMUTABILTY=\"yes\"/g" ${INSTALL_DIR}/first-start.sh
    rm ${TARGET_DIR}/etc/cron.daily/logrotate
    eend 0
}

configure_modules() {
    # Install modules utilities
    ebegin "Install modules control"
    install_pkg_list MODULES
    eend 0

    ebegin "Configure modules loading"
    echo "kernel.modules_disabled = 1" > ${TARGET_DIR}/etc/sysctl.d/06-modules.conf
    eend 0
}

configure_hids() {
    # Install and configure AIDE HIDS
    ebegin "Install AIDE hids"
    install_pkg_list HIDS
    eend 0

    ebegin "Configure AIDE hids"
    cp ${INSTALL_CFG_DIR}/hids/aide.conf ${TARGET_DIR}/etc/aide/
    cp ${INSTALL_CFG_DIR}/hids/aide.sh ${TARGET_DIR}/etc/cron.daily/
    chroot ${TARGET_DIR} /bin/bash \
        -c "chmod u+x /etc/cron.daily/aide.sh"
    chroot ${TARGET_DIR} /bin/bash \
        -c "aide --init --config /etc/aide/aide.conf"
    cp ${TARGET_DIR}/var/lib/aide/aide.db.new ${TARGET_DIR}/var/lib/aide/aide.db
    eend 0
}

configure_rbac() {
    # Install and onfigure gradm and acl
    echo "In works"
    ebegin "Install RBAC manager"
    install_pkg_list RBAC
    eend 0

    ebegin "RBAC configuration"
    sed -i -e "s/GRSEC_RBAC=\".*\"/GRSEC_RBAC=\"yes\"/g" ${INSTALL_DIR}/first-start.sh
    eend 0
}

configure_rootkit() {
    # Install and configure rootkit
    # putting them in a CRON tab
    ebegin "Install rootkit detector"
    install_pkg_list ROOTKIT
    eend 0

    ebegin "Configure rootkit detector"
    cp ${INSTALL_CFG_DIR}/rootkit/chkrootkit ${TARGET_DIR}/etc/cron.daily/
    chroot ${TARGET_DIR} /bin/bash \
        -c "chmod u+x /etc/cron.daily/chkrootkit"
    mkdir -p ${TARGET_DIR}/var/lib/rkhunter/tmp
    sed -i -e "s/ENABLE=no/ENABLE=yes/g" ${TARGET_DIR}/etc/cron.daily/rkhunter
    sed -i -e "s/LOG=no/LOG=yes/g" ${TARGET_DIR}/etc/cron.daily/rkhunter
    sed -i -e "s/^#LOGFILE_PERMS/LOGFILE_PERMS/g" ${TARGET_DIR}/etc/cron.daily/rkhunter
    eend 0
}

clean_target() {
    # Remove all undesired files
    ebegin "Clean target"
    rm -rf ${TARGET_DIR}/var/log/lxc/*

    rm -rf ${TARGET_DIR}/usr/portage

    rm -rf ${TARGET_DIR}/usr/bin/ebuild
    rm -rf ${TARGET_DIR}/usr/bin/egencache
    rm -rf ${TARGET_DIR}/usr/bin/emerge*
    rm -rf ${TARGET_DIR}/usr/bin/emirrordist
    rm -rf ${TARGET_DIR}/usr/bin/portageq
    rm -rf ${TARGET_DIR}/usr/bin/quickpkg
    rm -rf ${TARGET_DIR}/usr/bin/repoman

    rm -rf ${TARGET_DIR}/usr/lib/portage
    rm -rf ${TARGET_DIR}/usr/lib/python-exec
    rm -rf ${TARGET_DIR}/usr/lib64/python2.7/site-packages
    rm -rf ${TARGET_DIR}/usr/lib64/python3.3/site-packages

    rm -rf ${TARGET_DIR}/usr/sbin/archive-conf
    rm -rf ${TARGET_DIR}/usr/sbin/dispatch-conf
    rm -rf ${TARGET_DIR}/usr/sbin/emaint
    rm -rf ${TARGET_DIR}/usr/sbin/env-update
    rm -rf ${TARGET_DIR}/usr/sbin/etc-update
    rm -rf ${TARGET_DIR}/usr/sbin/fixpackages
    rm -rf ${TARGET_DIR}/usr/sbin/regenworld

    rm -rf ${TARGET_DIR}/var/log/portage
    eend 0
}

base_install() {
    # Meta function to install all
    # base components
    detect_characteristics
    create_parts
    create_lvm
    mount_parts
    install_kernel
    create_device
    configure_portage
    install_base
}

base_configuration() {
    # Meta function to configure
    # all base components
    configure_grub
    split_etc
    configure_initramfs
    configure_system
    configure_security
    configure_network
    configure_capabilities
    configure_pam
    create_users_groups
    configure_host_syslog
}

services_install() {
    # Meta funtion to test if services
    # are required and intall them in this 
    # case
    install_editor
    choose_ip BRIDGE \
        && choose_netmask BRIDGE \
        && configure_lxc ${IP_ADDR} ${NETMASK}
    test ${APACHE} = "on" \
        && choose_ip APACHE \
        && create_busybox_container APACHE ${IP_ADDR} \
        && install_service APACHE \
        && configure_apache ${IP_ADDR}
    test ${CENTOS} = "on" \
        && choose_ip CENTOS \
        && create_os_container CENTOS ${IP_ADDR} 7
    test ${GENTOO} = "on" \
        && choose_ip GENTOO \
        && create_os_container GENTOO ${IP_ADDR} current
    test ${HTTPD} = "on" \
        && choose_ip HTTPD \
        && create_busybox_container HTTPD ${IP_ADDR} \
        && configure_httpd ${IP_ADDR}
    test ${NGINX} = "on" \
        && choose_ip NGINX \
        && create_busybox_container NGINX ${IP_ADDR} \
        && install_service NGINX \
        && configure_nginx ${IP_ADDR}
    test ${NTP} = "on" \
        && choose_ip NTP \
        && create_busybox_container NTP ${IP_ADDR} \
        && install_service NTP \
        && configure_ntp ${IP_ADDR}
    test ${SAMBA} = "on" \
        && choose_ip SAMBA \
        && create_busybox_container SAMBA ${IP_ADDR} \
        && install_service SAMBA \
        && configure_samba ${IP_ADDR}
    test ${SSHD} = "on" \
        && choose_ip SSHD \
        && configure_sshd ${IP_ADDR}
    test ${SYSLOG} = "on" \
        && choose_ip SYSLOG \
        && create_busybox_container SYSLOG ${IP_ADDR} \
        && install_service SYSLOG \
        && configure_syslog
    test ${UBUNTU} = "on" \
        && choose_ip UBUNTU \
        && create_os_container UBUNTU ${IP_ADDR} trusty
}

security_install() {
    # Meta funtion to test if services
    # are required and intall them in this 
    # case
    set_acl
    test ${FIREWALL} = "on" \
        && configure_firewall
    test ${HIDS} = "on" \
        && configure_hids
    test ${LOG_IMU} = "on" \
        && protect_log
    test ${LESS_ROOT} = "on" \
        && disable_root
    test ${MODULES} = "on" \
        && configure_modules
    test ${PAXCTL} = "on" \
        && configure_pax
    test ${RBAC} = "on" \
        && configure_rbac
    test ${ROOTKIT} = "on" \
        && configure_rootkit
}

update_pkg() {
    configure_portage
    install_base
}

upgrade_kernel() {
    install_kernel
}

ended_install() {
    # Meta funtion to finish the 
    # installation
    cp first-start.sh ${TARGET_DIR}/var/res
    cp security_check.sh ${TARGET_DIR}/var/res
    echo 'sh /var/res/first-start.sh' >> ${TARGET_DIR}/etc/bashrc
    clean_target
    umount_parts
}

###################
# DEBUG FUNCTIONS #
###################
print_install_var() {
    echo "Installation root directory ${INSTALL_DIR}"
    echo "Installation configuration directory ${INSTALL_CFG_DIR}"
    echo "Installation portage configuration directory ${INSTALL_POR_DIR}"
    echo "Installation package directory ${INSTALL_PKG_DIR}"
    echo "Target directory ${TARGET_DIR}"

}

print_conf_var () {
    while read line; do
        [ -z "${line}" ] && continue
        [[ "${line}" =~ ^#.*$ ]] && continue
        echo ${line}
    done < ./conf/hardened_factory.cfg
}

print_calculated_var() {
    echo "Arch ${ARCH}"
    echo "Disk size ${SIZE_DISK}"
    echo "Ram size ${SIZE_RAM}"
    echo "SWAP size ${SIZE_SWAP}"
    echo "Available interfaces ${AVAILABLE_INTERFACES}"
}

#detect_characteristics
#create_parts
#create_lvm
#mount_parts
#install_kernel
#create_device
#configure_portage
#install_base
#configure_grub
#split_etc
#configure_initramfs
#configure_system
#configure_security
#configure_network
#configure_capabilities
#configure_pam
#create_users_groups
#configure_host_syslog
#install_editor
#choose_ip BRIDGE \
#    && choose_netmask BRIDGE \
#    && configure_lxc ${IP_ADDR} ${NETMASK}
#test ${APACHE} = "on" \
#    && choose_ip APACHE \
#    && create_busybox_container APACHE ${IP_ADDR} \
#    && install_service APACHE \
#    && configure_apache ${IP_ADDR}
#test ${CENTOS} = "on" \
#    && choose_ip CENTOS \
#    && create_os_container CENTOS ${IP_ADDR} 7
#test ${GENTOO} = "on" \
#    && choose_ip GENTOO \
#    && create_os_container GENTOO ${IP_ADDR} current
#test ${HTTPD} = "on" \
#    && choose_ip HTTPD \
#    && create_busybox_container HTTPD ${IP_ADDR} \
#    && configure_httpd ${IP_ADDR}
#test ${NGINX} = "on" \
#    && choose_ip NGINX \
#    && create_busybox_container NGINX ${IP_ADDR} \
#    && install_service NGINX \
#    && configure_nginx ${IP_ADDR}
#test ${NTP} = "on" \
#    && choose_ip NTP \
#    && create_busybox_container NTP ${IP_ADDR} \
#    && install_service NTP \
#    && configure_ntp ${IP_ADDR}
#test ${SAMBA} = "on" \
#    && choose_ip SAMBA \
#    && create_busybox_container SAMBA ${IP_ADDR} \
#    && install_service SAMBA \
#    && configure_samba ${IP_ADDR}
#test ${SSHD} = "on" \
#    && choose_ip SSHD \
#    && configure_sshd ${IP_ADDR}
#test ${SYSLOG} = "on" \
#    && choose_ip SYSLOG \
#    && create_busybox_container SYSLOG ${IP_ADDR} \
#    && install_service SYSLOG \
#    && configure_syslog
#test ${UBUNTU} = "on" \
#    && choose_ip UBUNTU \
#    && create_os_container UBUNTU ${IP_ADDR} trusty_syslog
#set_acl
#test ${RBAC} = "on" \
#    && configure_rbac
#test ${FIREWALL} = "on" \
#    && configure_firewall
#test ${HIDS} = "on" \
#    && configure_hids
#test ${LOG_IMU} = "on" \
#    && protect_log
#test ${LESS_ROOT} = "on" \
#    && disable_root
#test ${MODULES} = "on" \
#    && configure_modules
#test ${PAXCTL} = "on" \
#    && configure_pax
#test ${ROOTKIT} = "on" \
#    && configure_rootkit
#ended_install

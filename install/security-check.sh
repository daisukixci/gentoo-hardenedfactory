#!/bin/bash

# global vars
have_readelf=1
verbose=false

# FORTIFY_SOURCE vars
FS_end=_chk
FS_cnt_total=0
FS_cnt_checked=0
FS_cnt_unchecked=0
FS_chk_func_libc=0
FS_functions=0
FS_libc=0

command_exists () {
    type $1  > /dev/null 2>&1;
}

search_wrong_perm() {
    # search world writable or group writable
    find / -type f \( -perm -2 -o -perm -20 \) -exec ls -lg {} \; 
    find / -type d \( -perm -2 -o -perm -20 \) -exec ls -ldg {} \; 
    find / -type d \( -perm -g+w -o perm -o+w \) not -perm -a+t -exec ls -lad {} \;
    find / -xdev \( -nouser -o -nogroup \) -print
}

search_suid() {
    # search for root owned file with suid
    find / -user root -perm -4000 -exec ls -ldb {} \;
    # search for suid and guid file
    find / -type f \( -perm -4000 -o -perm -2000 \) -exec ls -lg {} \; 
    find / -type f \( -perm -4000 -o -perm -2000 \) -links +1 -ls 
    # determine if they are script or binaries
    find / \( -perm -4000 -o -perm -2000 \) -type f -exec file {} \; | grep -v ELF
}

check_no_empty_passwd(){
    awk -F: '($2 == "") {print}' /etc/shadow
}

# check for kernel protection mechanisms
check_kernel() {
    if [ -f /proc/config.gz ] ; then
        kconfig="zcat /proc/config.gz"
        echo "/proc/config.gz\n\n"
    elif [ -f /boot/config-`uname -r` ] ; then
        kconfig="cat /boot/config-`uname -r`"
        echo "/boot/config-`uname -r`\n\n"
        echo "  Warning: The config on disk may not represent running kernel config!\n\n";
    elif [ -f "${KBUILD_OUTPUT:-/usr/src/linux}"/.config ] ; then
        kconfig="cat ${KBUILD_OUTPUT:-/usr/src/linux}/.config"
        echo "%s\n\n" "${KBUILD_OUTPUT:-/usr/src/linux}/.config"
        echo "  Warning: The config on disk may not represent running kernel config!\n\n";
    else
        echo "NOT FOUND\n\n"
        exit 0
    fi

    printf "GCC stack protector support;" >> ${FILE}
    if $kconfig | grep -qi 'CONFIG_CC_STACKPROTECTOR=y'; then
        printf "Enabled\n" >> ${FILE}
    else
        printf "Disabled\n" >> ${FILE}
    fi

    printf "Strict user copy checks;" >> ${FILE}
    if $kconfig | grep -qi 'CONFIG_DEBUG_STRICT_USER_COPY_CHECKS=y'; then
        printf "Enabled\n" >> ${FILE}
    else
        printf "Disabled\n" >> ${FILE}
    fi

    printf "Enforce read-only kernel data;" >> ${FILE}
    if $kconfig | grep -qi 'CONFIG_DEBUG_RODATA=y'; then
        printf "Enabled\n" >> ${FILE}
    else
        printf "Disabled\n" >> ${FILE}
    fi
    printf "Restrict /dev/mem access;" >> ${FILE}
    if $kconfig | grep -qi 'CONFIG_STRICT_DEVMEM=y'; then
        printf "Enabled\n" >> ${FILE}
    else
        printf "Disabled\n" >> ${FILE}
    fi

    printf "Restrict /dev/kmem access;" >> ${FILE}
    if $kconfig | grep -qi 'CONFIG_DEVKMEM=y'; then
        printf "Disabled\n" >> ${FILE}
    else
        printf "Enabled\n" >> ${FILE}
    fi

    printf "\n" >> ${FILE}
    printf "GrSecurity / PaX;" >> ${FILE}

    if $kconfig | grep -qi 'CONFIG_GRKERNSEC=y'; then
        printf "Enabled;" >> ${FILE}
        if $kconfig | grep -qi 'CONFIG_GRKERNSEC_HIGH=y'; then
            printf "High GRKERNSEC\n" >> ${FILE}
        elif $kconfig | grep -qi 'CONFIG_GRKERNSEC_MEDIUM=y'; then
            printf "Medium GRKERNSEC\n" >> ${FILE}
        elif $kconfig | grep -qi 'CONFIG_GRKERNSEC_LOW=y'; then
            printf "Low GRKERNSEC\n" >> ${FILE}
        else
            printf "Custom GRKERNSEC\n" >> ${FILE}
        fi

        printf "Non-executable kernel pages;" >> ${FILE}
        if $kconfig | grep -qi 'CONFIG_PAX_KERNEXEC=y'; then
            printf "Enabled\n" >> ${FILE}
        else
            printf "Disabled\n" >> ${FILE}
        fi

        printf "Prevent userspace pointer deref;" >> ${FILE}
        if $kconfig | grep -qi 'CONFIG_PAX_MEMORY_UDEREF=y'; then
            printf "Enabled\n" >> ${FILE}
        else
            printf "Disabled\n" >> ${FILE}
        fi

        printf "Prevent kobject refcount overflow;" >> ${FILE}
        if $kconfig | grep -qi 'CONFIG_PAX_REFCOUNT=y'; then
            printf "Enabled\n" >> ${FILE}
        else
            printf "Disabled\n" >> ${FILE}
        fi

        printf "Bounds check heap object copies;" >> ${FILE}
        if $kconfig | grep -qi 'CONFIG_PAX_USERCOPY=y'; then
            printf "Enabled\n" >> ${FILE}
        else
            printf "Disabled\n" >> ${FILE}
        fi

        printf "Disable writing to kmem/mem/port;" >> ${FILE}
        if $kconfig | grep -qi 'CONFIG_GRKERNSEC_KMEM=y'; then
            printf "Enabled\n" >> ${FILE}
        else
            printf "Disabled\n" >> ${FILE}
        fi

        printf "Disable privileged I/O;" >> ${FILE}
        if $kconfig | grep -qi 'CONFIG_GRKERNSEC_IO=y'; then
            printf "Enabled\n" >> ${FILE}
        else
            printf "Disabled\n" >> ${FILE}
        fi

        printf "Harden module auto-loading;" >> ${FILE}
        if $kconfig | grep -qi 'CONFIG_GRKERNSEC_MODHARDEN=y'; then
            printf "Enabled\n" >> ${FILE}
        else
            printf "Disabled\n" >> ${FILE}
        fi

        printf "Hide kernel symbols;" >> ${FILE}
        if $kconfig | grep -qi 'CONFIG_GRKERNSEC_HIDESYM=y'; then
            printf "Enabled\n" >> ${FILE}
        else
            printf "Disabled\n" >> ${FILE}
        fi
    else
        printf "No GRKERNSEC\n" >> ${FILE}
    fi
}

# check file(s)
filecheck() {
    # check for RELRO support
    if readelf -l $1 2>/dev/null | grep -q 'GNU_RELRO'; then
        if readelf -d $1 2>/dev/null | grep -q 'BIND_NOW'; then
            printf 'Full RELRO;' >> ${FILE}
        else
            printf 'Partial RELRO;' >> ${FILE}
        fi
    else
        printf 'No RELRO;' >> ${FILE}
    fi

    # check for stack canary support
    if readelf -s $1 2>/dev/null | grep -q '__stack_chk_fail'; then
        printf 'Canary found;' >> ${FILE}
    else
        printf 'No canary found;' >> ${FILE}
    fi

    # check for NX support
    if readelf -W -l $1 2>/dev/null | grep 'GNU_STACK' | grep -q 'RWE'; then
        printf 'NX disabled;' >> ${FILE}
    else
        printf 'NX enabled;' >> ${FILE}
    fi 

    # check for PIE support
    if readelf -h $1 2>/dev/null | grep -q 'Type:[[:space:]]*EXEC'; then
        printf 'No PIE;' >> ${FILE}
    elif readelf -h $1 2>/dev/null | grep -q 'Type:[[:space:]]*DYN'; then
        if readelf -d $1 2>/dev/null | grep -q '(DEBUG)'; then
            printf 'PIE enabled;' >> ${FILE}
        else   
            printf 'DSO;' >> ${FILE}
        fi
    else
        printf 'Not an ELF file;' >> ${FILE}
    fi 

    # check for rpath / run path
    if readelf -d $1 2>/dev/null | grep -q 'rpath'; then
        printf 'RPATH;' >> ${FILE}
    else
        printf 'No RPATH;' >> ${FILE}
    fi

    if readelf -d $1 2>/dev/null | grep -q 'runpath'; then
        printf 'RUNPATH\n' >> ${FILE}
    else
        printf 'No RUNPATH\n' >> ${FILE}
    fi
}

# check process(es)
proccheck() {
    # check for RELRO support
    if readelf -l $1/exe 2>/dev/null | grep -q 'Program Headers'; then
        if readelf -l $1/exe 2>/dev/null | grep -q 'GNU_RELRO'; then
            if readelf -d $1/exe 2>/dev/null | grep -q 'BIND_NOW'; then
                printf 'Full RELRO;' >> ${FILE}
            else
                printf 'Partial RELRO;' >> ${FILE}
            fi
        else
            printf 'No RELRO;' >> ${FILE}
        fi
    fi

    # check for stack canary support
    if readelf -s $1/exe 2>/dev/null | grep -q 'Symbol table'; then
        if readelf -s $1/exe 2>/dev/null | grep -q '__stack_chk_fail'; then
            printf 'Canary found;' >> ${FILE}
        else
            printf 'No canary found;' >> ${FILE}
        fi
    else
        if [ "$1" != "1" ] ; then
            echo 'proccheck: Permission denied'
        else
            echo 'proccheck: No symbol table found'
        fi
    fi

    # first check for PaX support
    if cat $1/status 2> /dev/null | grep -q 'PaX:'; then
        pageexec=( $(cat $1/status 2> /dev/null | grep 'PaX:' | cut -b6) )
        segmexec=( $(cat $1/status 2> /dev/null | grep 'PaX:' | cut -b10) )
        mprotect=( $(cat $1/status 2> /dev/null | grep 'PaX:' | cut -b8) )
        randmmap=( $(cat $1/status 2> /dev/null | grep 'PaX:' | cut -b9) )
        if [[ "$pageexec" = "P" || "$segmexec" = "S" ]] && [[ "$mprotect" = "M" && "$randmmap" = "R" ]] ; then
            printf 'PaX enabled;' >> ${FILE}
        elif [[ "$pageexec" = "p" && "$segmexec" = "s" && "$randmmap" = "R" ]] ; then
            printf 'PaX ASLR only;' >> ${FILE}
        elif [[ "$pageexec" = "P" || "$segmexec" = "S" ]] && [[ "$mprotect" = "m" && "$randmmap" = "R" ]] ; then
            printf 'PaX mprot off;' >> ${FILE}
        elif [[ "$pageexec" = "P" || "$segmexec" = "S" ]] && [[ "$mprotect" = "M" && "$randmmap" = "r" ]] ; then
            printf 'PaX ASLR off;' >> ${FILE}
        elif [[ "$pageexec" = "P" || "$segmexec" = "S" ]] && [[ "$mprotect" = "m" && "$randmmap" = "r" ]] ; then
            printf 'PaX NX only;' >> ${FILE}
        else
            printf 'PaX disabled;' >> ${FILE}
        fi
        # fallback check for NX support
    elif readelf -W -l $1/exe 2>/dev/null | grep 'GNU_STACK' | grep -q 'RWE'; then
        printf 'NX disabled;' >> ${FILE}
    else
        printf 'NX enabled;' >> ${FILE}
    fi 

    # check for PIE support
    if readelf -h $1/exe 2>/dev/null | grep -q 'Type:[[:space:]]*EXEC'; then
        printf 'No PIE;' >> ${FILE}
    elif readelf -h $1/exe 2>/dev/null | grep -q 'Type:[[:space:]]*DYN'; then
        if readelf -d $1/exe 2>/dev/null | grep -q '(DEBUG)'; then
            printf 'PIE enabled;' >> ${FILE}
        else   
            printf 'Dynamic Shared Object;' >> ${FILE}
        fi
    else
        echo -n -e 'proccheck: Not an ELF file'
    fi
}

# check cpu nx flag
nxcheck() {
    if grep -q nx /proc/cpuinfo; then
        printf 'Yes\n' >> ${FILE}
    else
        printf 'No\n' >> ${FILE}
    fi
}

aslrcheck() {
    # PaX ASLR support
    if !(cat /proc/1/status 2> /dev/null | grep -q 'Name:') ; then
        echo -n -e ': insufficient privileges for PaX ASLR checks\n'
        echo -n -e '  Fallback to standard Linux ASLR check'
    fi

    if cat /proc/1/status 2> /dev/null | grep -q 'PaX:'; then
        printf ";" >> ${FILE}
        if cat /proc/1/status 2> /dev/null | grep 'PaX:' | grep -q 'R'; then
            printf 'PaX ASLR enabled\n' >> ${FILE}
        else
            printf 'PaX ASLR disabled\n' >> ${FILE}
        fi
    else
        # standard Linux 'kernel.randomize_va_space' ASLR support
        # (see the kernel file 'Documentation/sysctl/kernel.txt' for a detailed description)
        printf " (kernel.randomize_va_space): "
        if /sbin/sysctl -a 2>/dev/null | grep -q 'kernel.randomize_va_space = 1'; then
            echo -n -e 'On (Setting: 1)\n\n'
            printf "  Description - Make the addresses of mmap base, stack and VDSO page randomized.\n"
            printf "  This, among other things, implies that shared libraries will be loaded to \n"
            printf "  random addresses. Also for PIE-linked binaries, the location of code start\n"
            printf "  is randomized. Heap addresses are *not* randomized.\n\n"
        elif /sbin/sysctl -a 2>/dev/null | grep -q 'kernel.randomize_va_space = 2'; then
            echo -n -e 'On (Setting: 2)\n\n'
            printf "  Description - Make the addresses of mmap base, heap, stack and VDSO page randomized.\n"
            printf "  This, among other things, implies that shared libraries will be loaded to random \n"
            printf "  addresses. Also for PIE-linked binaries, the location of code start is randomized.\n\n"
        elif /sbin/sysctl -a 2>/dev/null | grep -q 'kernel.randomize_va_space = 0'; then
            echo -n -e 'Off (Setting: 0)\n'
        else
            echo -n -e 'Not supported\n'
        fi
        printf "  See the kernel file 'Documentation/sysctl/kernel.txt' for more details.\n\n"
    fi 
}

check_binary_folder() {
    if [ $have_readelf -eq 0 ] ; then
        exit 1
    fi
    if [ -z "$2" ] ; then
        echo -n -e "check_binary_folder: Error: Please provide a valid directory"
        exit 1
    fi
    # remove trailing slashes
    tempdir=`echo $2 | sed -e "s/\/*$//"`
    if [ ! -d $tempdir ] ; then
        echo "check_binary_folder: Error: The directory '$tempdir' does not exist"
        exit 1
    fi
    cd $tempdir
    printf "RELRO;STACK CANARY;NX;PIE;RPATH;RUNPATH;FILE\n" >> ${FILE}
    echo TEST
    for N in [A-Za-z]*; do
        if [ "$N" != "[A-Za-z]*" ]; then
            # read permissions?
            if [ ! -r $N ]; then
                echo -n -e "check_binary_folder: Error: No read permissions for '$tempdir/$N' (run as root)"
            else
                # ELF executable?
                out=`file $N`
                if [[  $out =~ ELF ]] ; then
                    filecheck $N
                    if [ `find $tempdir/$N \( -perm -004000 -o -perm -002000 \) -type f -print` ]; then
                        printf ";%s;%s\n" $2 $N >> ${FILE}
                    else
                        printf ";%s;%s\n" $tempdir/ $N >> ${FILE}
                    fi
                fi
            fi
        fi
    done
}

check_proc() {
    if [ $have_readelf -eq 0 ] ; then
        exit 1
    fi
    cd /proc
    printf "System-wide ASLR;" >> ${FILE}
    aslrcheck
    printf "Does the CPU support NX;" >> ${FILE}
    nxcheck 
    printf "COMMAND;PID RELRO;STACK CANARY;NX/PaX;PIE\n" >> ${FILE}
    for N in [1-9]*; do
        if [ $N != $$ ] && readlink -q $N/exe > /dev/null; then
            printf "%16s" `head -1 $N/status | cut -b 7-`
            printf "%7d " $N
            proccheck $N
            echo
        fi
    done
}

############
### MAIN ###
############
FILE="/root/security_check.csv"
test -f ${FILE} && rm ${FILE}
if !(command_exists readelf) ; then
    echo -n -e "Warning: 'readelf' not found! It's required for most checks.\n\n"
    have_readelf=0
fi

for dir in /bin /sbin/ /usr/bin /usr/sbin; do
    check_binary_folder --dir ${dir}
done
check_proc
check_kernel
search_suid
search_wrong_perm


#!/bin/bash

# @Purpose: Provide a GUI to 
# deploy the hardened OS
#
# @Author: PIQUENOT Gaetan
# @Property: AIRBUS DEFENSE AND SPACE 
# @Licence:
# @Copyright:

FACTORY_CFG="./conf/hardened_factory.cfg"
source ./install_hardos.sh

DIALOG_DIM="25 70 25"

wizard() {
    # A simple menu to provide quick acces to install
    trap 'delete_menu_tmp'  EXIT 

    while :
    do
        dialog --clear --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "MAIN MENU" \
            --menu "Use [UP/DOWN] key to move" ${DIALOG_DIM} \
            "Display"           "Display actual configuration" \
            "Installation"      "Launch the installation process" \
            "Update"            "Update your installed packages" \
            "Upgrade"           "Upgrade your already installed OS" \
            "Mount"             "Mount partitions only for debugging after install process" \
            "Unmount"           "Umount partitions after debugging" \
            "Reboot"            "After install reboot" \
            "Exit"              "To exit" 2> menuchoices.main

        retopt=${?}
        choice=`cat menuchoices.main`

        case ${retopt} in
            0) 
                case ${choice} in
                    Display)
                        dialog --textbox ${FACTORY_CFG} 80 100
                        ;;
                    Installation)
                        base_install
                        base_configuration
                        services_install
                        security_install
                        ended_install
                        ;;
                    Update)
                        mount_parts
                        update_pkg
                        umount_parts
                        ;;
                    Upgrade)
                        mount_parts
                        upgrade_kernel
                        update_pkg
                        umount_parts
                        ;;
                    Mount)
                        mount_parts
                        ;;
                    Unmount)
                        umount_parts
                        ;;
                    Reboot)
                        reboot
                        clear
                        exit 0
                        ;;
                    Exit)
                        clear;
                        exit 0
                        ;;
                    *)
                        ;;
                esac
                ;;
            1)
                clear
                break
                ;;
            *)
                clear
                echo "Something gone wrong"
                break
                ;;
        esac
    done
}

delete_menu_tmp(){
    # Delete temporary files
    rm -f menuchoices.*
}

wizard

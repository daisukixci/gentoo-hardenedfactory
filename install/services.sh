#!/bin/bash

# @Purpose: Provide generic functions to 
# deploy containers and here there are
# specific functions to configurate
# containers
#
# @Author: PIQUENOT Gaetan
# @Property: AIRBUS DEFENSE AND SPACE 
# @Licence:
# @Copyright:

LXC_PATH="srv/lxc"

############################
# Generic installation and #
# configuration section    #
############################

create_busybox_container(){
    # Create a busybox container according parameters 
    # provides into function call
    # create_busybox_container SERVICE_NAME SERVICE_IP
    # and add it to the boot level
    SERVICE_NAME=`echo ${1} | tr '[:upper:]' '[:lower:]'`
    ebegin "Create ${SERVICE_NAME} container"

    sed -i -e "s/container_veth/${SERVICE_NAME}iface/" ${TARGET_DIR}/etc/lxc/guest.conf
    sed -i -e "s/container_ip/${2}/" ${TARGET_DIR}/etc/lxc/guest.conf

    while true; do
        ewarn "Choose the container size. ex: 1G for one gigabyte, 500M for 500 megabytes"
        read SERVICE_SIZE
        if [[ ${SERVICE_SIZE} =~ [0-9]+G|[0-9]+M ]]; then
            break
        fi
        eerror "Invalid size"
    done

    mkdir -p ${TARGET_DIR}/${LXC_PATH}/${SERVICE_NAME}

    chroot ${TARGET_DIR} /bin/bash \
        -c "lxc-create -n ${SERVICE_NAME} \
        -t busybox -f etc/lxc/guest.conf \
        -B lvm --vgname vg --fssize ${SERVICE_SIZE}"
    chroot ${TARGET_DIR} /bin/bash \
        -c "mount /dev/mapper/vg-${SERVICE_NAME} /${LXC_PATH}/${SERVICE_NAME}"

    sed -i -e "s/${SERVICE_NAME}iface/container_veth/" ${TARGET_DIR}/etc/lxc/guest.conf
    sed -i -e "s/${2}/container_ip/" ${TARGET_DIR}/etc/lxc/guest.conf

    cd ${TARGET_DIR}/etc/init.d/
    ln -s lxc ${TARGET_DIR}/etc/init.d/lxc.${SERVICE_NAME}
    cd ${INSTALL_DIR}
    sed -i -e "s:/bin/udhcpc:/bin/ifconfig eth0 ${2}:g" ${TARGET_DIR}/${LXC_PATH}/${SERVICE_NAME}/etc/init.d/rcS
    chroot ${TARGET_DIR} /bin/bash \
        -c "ln -s /var/lib/lxc/${SERVICE_NAME}/config /etc/lxc/${SERVICE_NAME}.conf"
    chroot ${TARGET_DIR} /bin/bash \
        -c "rc-update add lxc.${SERVICE_NAME} default"
    cp ${TARGET_DIR}/etc/resolv.conf ${TARGET_DIR}/${LXC_PATH}/${SERVICE_NAME}/etc/
    sed -i -e "s/lxc\.mount\.entry = \/usr/#lxc\.mount\.entry = \/usr/g" ${TARGET_DIR}/var/lib/lxc/${SERVICE_NAME}/config
    sed -i -e "s:/bin/syslogd::g" ${TARGET_DIR}/${LXC_PATH}/${SERVICE_NAME}/etc/init.d/rcS
    chroot ${TARGET_DIR} /bin/bash -c "umount /dev/mapper/vg-${SERVICE_NAME}"
    eend 0
}

create_os_container() {
    # Create an OS container according parameters 
    # provides into function call
    # create_os_container SERVICE_NAME SERVICE_IP RELEASE
    # and add it to the boot level
    SERVICE_NAME=`echo ${1} | tr '[:upper:]' '[:lower:]'`
    ebegin "Configure ${SERVICE_NAME} container"

    mkdir -p ${TARGET_DIR}/var/cache/lxc/download
    cp -a ${INSTALL_DIR}/res/download/${SERVICE_NAME} ${TARGET_DIR}/var/cache/lxc/download/
    sed -i -e "s/container_veth/${SERVICE_NAME}iface/" ${TARGET_DIR}/etc/lxc/guest.conf
    sed -i -e "s/container_ip/${2}/" ${TARGET_DIR}/etc/lxc/guest.conf
    case ${ARCH} in
        x86_64)
            CONTAINER_ARCH=amd64
            ;;
        *)
            eerror "${ARCH} not implemented"
            ;;
    esac

    while true; do
        ewarn "Choose the container size. ex: 1G for one gigabyte, 500M for 500 megabytes"
        read SERVICE_SIZE
        if [[ ${SERVICE_SIZE} =~ [0-9]+G|[0-9]+M ]]; then
            break
        fi
        eerror "Invalid size"
    done

    mkdir -p ${TARGET_DIR}/${LXC_PATH}/${SERVICE_NAME}
    chroot ${TARGET_DIR} /bin/bash \
        -c "lxc-create -n ${SERVICE_NAME} \
        -t download -f etc/lxc/guest.conf \
        -B lvm --vgname vg --fssize ${SERVICE_SIZE} \
        -- -d ${SERVICE_NAME} -r ${3} -a ${CONTAINER_ARCH} \
        --no-validate --force-cache"
    chroot ${TARGET_DIR} /bin/bash \
        -c "mount /dev/mapper/vg-${SERVICE_NAME} /${LXC_PATH}/${SERVICE_NAME}"
    cp ${TARGET_DIR}/etc/resolv.conf ${TARGET_DIR}/${LXC_PATH}/${SERVICE_NAME}/etc/

    sed -i -e "s/${SERVICE_NAME}iface/container_veth/" ${TARGET_DIR}/etc/lxc/guest.conf
    sed -i -e "s/${2}/container_ip/" ${TARGET_DIR}/etc/lxc/guest.conf
    cd ${TARGET_DIR}/etc/init.d/
    ln -s lxc ${TARGET_DIR}/etc/init.d/lxc.${SERVICE_NAME}
    cd ${INSTALL_DIR}
    chroot ${TARGET_DIR} /bin/bash \
        -c "ln -s /var/lib/lxc/${SERVICE_NAME}/config /etc/lxc/${SERVICE_NAME}.conf"
    chroot ${TARGET_DIR} /bin/bash \
        -c "rc-update add lxc.${SERVICE_NAME} default"
    chroot ${TARGET_DIR} /bin/bash \
        -c "umount /dev/mapper/vg-${SERVICE_NAME}"
    eend 0
}

install_service() {
    # Install service according to parameters provided into function call
    # install_service SERVICE_NAME
    # The installation is directly into the container 
    # according to the ROOT emerge variable
    SERVICE_NAME=`echo ${1} | tr '[:upper:]' '[:lower:]'`

    ebegin "Installation of ${SERVICE_NAME}"
    export PORTAGE_TMPDIR=${TARGET_DIR}/var/tmp
    export PKGDIR=${INSTALL_PKG_DIR}

    ROOT=${TARGET_DIR} eselect news read all 1>/dev/null 

    PKG_INLINE=""
    PKG_LIST_NAME="${1}_PKG"
    PKG_LIST=${!PKG_LIST_NAME}
    if [ "${!1}" = on ]; then
        for i in ${PKG_LIST[@]}; do
            PKG_INLINE="${i} "
        done
    fi

    chroot ${TARGET_DIR} /bin/bash \
        -c "mount /dev/mapper/vg-${SERVICE_NAME} /${LXC_PATH}/${SERVICE_NAME}"
    [ -n "${PKG_INLINE}" ] \
        && ROOT=${TARGET_DIR}/${LXC_PATH}/${SERVICE_NAME} \
        PORTAGE_CONFIGROOT=${TARGET_DIR} \
        emerge -qK ${PKG_INLINE}
    chroot ${TARGET_DIR} /bin/bash \
        -c "umount /dev/mapper/vg-${SERVICE_NAME}"
    eend 0
}

##################################
# Services configuration section #
##################################

configure_apache() {
    # Configure apache creating a user
    # and creating directory
    ebegin "Configure apache"
    chroot ${TARGET_DIR} /bin/bash \
        -c "mount /dev/mapper/vg-apache /${LXC_PATH}/apache"
    cp ${INSTALL_DIR}/res/lib/${ARCH}/libgcc_s.so* ${TARGET_DIR}/${LXC_PATH}/apache/usr/lib

    echo "127.0.0.1 localhost apache" >> ${TARGET_DIR}/${LXC_PATH}/apache/etc/apache2/httpd.conf
    echo "/usr/sbin/apache2" >> ${TARGET_DIR}/${LXC_PATH}/apache/etc/init.d/rcS

    chroot ${TARGET_DIR}/${LXC_PATH}/apache/ /bin/sh \
        -c "addgroup apache"
    chroot ${TARGET_DIR}/${LXC_PATH}/apache/ /bin/sh \
        -c "adduser apache -G apache"

    #cp -R ${INSTALL_CFG_DIR}/lxc/httpd.conf ${TARGET_DIR}/${LXC_PATH}/apache/etc/apache2/
    cp -R ${INSTALL_CFG_DIR}/lxc/www ${TARGET_DIR}/${LXC_PATH}/apache/var/

    chroot ${TARGET_DIR}/${LXC_PATH}/apache/ /bin/sh \
        -c "chown -R apache /var/www"
    chroot ${TARGET_DIR}/${LXC_PATH}/apache/ /bin/sh \
        -c "chgrp -R apache /var/www"

    echo "iptables -t nat -A PREROUTING -p tcp \
        -i ${AVAILABLE_INTERFACES[0]} --dport 80 -j DNAT \
        --to-destination ${1}" >> ${INSTALL_CFG_DIR}/net/firewall-rules.sh
    chroot ${TARGET_DIR} /bin/bash \
        -c "umount /dev/mapper/vg-apache"
    eend 0
}

configure_httpd() {
    # Configure httpd with autostart
    ebegin "Configure httpd"
    chroot ${TARGET_DIR} /bin/bash \
        -c "mount /dev/mapper/vg-httpd /${LXC_PATH}/httpd"

    echo "/bin/httpd -p 80 -h /var/www/sample" >> ${TARGET_DIR}/${LXC_PATH}/${SERVICE_NAME}/etc/init.d/rcS

    mkdir -p ${TARGET_DIR}/${LXC_PATH}/${SERVICE_NAME}/var/www/sample
    echo '<html>It works !<\html>' > ${TARGET_DIR}/${LXC_PATH}/${SERVICE_NAME}/var/www/sample/index.html

    echo "iptables -t nat -A PREROUTING -p tcp -i ${AVAILABLE_INTERFACES[0]} --dport 80 -j DNAT --to-destination ${1}" >> ${INSTALL_CFG_DIR}/net/firewall-rules.sh
    chroot ${TARGET_DIR} /bin/bash \
        -c "umount /dev/mapper/vg-httpd"
    eend 0
}

configure_nginx() {
    # Configure nginx adding his dependencies
    # creating a user and providing
    # a hello world template
    ebegin "Nginx dependencies copy into container"
    chroot ${TARGET_DIR} /bin/bash \
        -c "mount /dev/mapper/vg-nginx /${LXC_PATH}/nginx"

    cp ${TARGET_DIR}/usr/lib64/libc* ${TARGET_DIR}/${LXC_PATH}/nginx/usr/lib64/
    cp ${TARGET_DIR}/lib/ld-linux* ${TARGET_DIR}/${LXC_PATH}/nginx/usr/lib64/
    cp ${TARGET_DIR}/usr/lib64/libdl* ${TARGET_DIR}/${LXC_PATH}/nginx/usr/lib64/
    cp ${TARGET_DIR}/usr/lib64/libpthread* ${TARGET_DIR}/${LXC_PATH}/nginx/usr/lib64/

    chroot ${TARGET_DIR}/${LXC_PATH}/nginx/ /bin/sh \
        -c "addgroup nginx"
    chroot ${TARGET_DIR}/${LXC_PATH}/nginx/ /bin/sh \
        -c "adduser nginx -G nginx"
    chroot ${TARGET_DIR}/${LXC_PATH}/nginx/ /bin/sh \
        -c "mkdir -p /var/www/localhost/htdocs"

    cp -R ${INSTALL_CFG_DIR}/lxc/nginx.conf ${TARGET_DIR}/${LXC_PATH}/nginx/etc/nginx/
    cp -R ${INSTALL_CFG_DIR}/lxc/www ${TARGET_DIR}/${LXC_PATH}/nginx/var/
    chroot ${TARGET_DIR}/${LXC_PATH}/nginx/ /bin/sh \
        -c "chown -R nginx /var/www"
    chroot ${TARGET_DIR}/${LXC_PATH}/nginx/ /bin/sh \
        -c "chgrp -R nginx /var/www"

    echo "/usr/sbin/nginx" >> ${TARGET_DIR}/${LXC_PATH}/nginx/etc/init.d/rcS

    echo "iptables -t nat -A PREROUTING -p tcp -i ${AVAILABLE_INTERFACES[0]} --dport 80 -j DNAT --to-destination ${1}" >> ${INSTALL_CFG_DIR}/net/firewall-rules.sh
    chroot ${TARGET_DIR} /bin/bash \
        -c "umount /dev/mapper/vg-nginx"
    eend 0
}

configure_ntp() {
    # Configure ntp into client per default
    # TODO
    echo "In works"
    ebegin "Configure ntp container"
    chroot ${TARGET_DIR} /bin/bash \
        -c "mount /dev/mapper/vg-ntp /${LXC_PATH}/ntp"

    cp ${INSTALL_DIR}/res/lib/${ARCH}/libgcc_s.so* ${TARGET_DIR}/${LXC_PATH}/ntp/usr/lib64
    cp ${INSTALL_CFG_DIR}/lxc/ntp.conf ${TARGET_DIR}/${LXC_PATH}/ntp/etc/
    echo "/usr/sbin/ntpd" >> ${TARGET_DIR}/${LXC_PATH}/ntp/etc/init.d/rcS

    chroot ${TARGET_DIR} /bin/bash \
        -c "umount /dev/mapper/vg-ntp"
    eend 0
}

configure_samba() {
    # Just prepare samba to be launched
    # default configuration will be useless
    # Adding a user to launch samba
    ebegin "Configure samba container"
    chroot ${TARGET_DIR} /bin/bash \
        -c "mount /dev/mapper/vg-samba /${LXC_PATH}/samba"

    cp ${TARGET_DIR}/${LXC_PATH}/samba/lib64/libkeyutils.so.1* ${TARGET_DIR}/${LXC_PATH}/samba/usr/lib64/
    cp ${INSTALL_CFG_DIR}/lxc/smb.conf ${TARGET_DIR}/${LXC_PATH}/samba/etc/samba/smb.conf

    chroot ${TARGET_DIR}/${LXC_PATH}/samba/ /bin/sh -c "addgroup nogroup"
    chroot ${TARGET_DIR}/${LXC_PATH}/samba/ /bin/sh -c "adduser nobody -G nogroup"

    echo "/usr/sbin/smbd" >> ${TARGET_DIR}/${LXC_PATH}/samba/etc/init.d/rcS

    chroot ${TARGET_DIR} /bin/bash -c "umount /dev/mapper/vg-samba"
    eend 0
}

configure_sshd(){
    # Installation of openssh
    # Put the IP address into configuration file
    # Create the container
    # Put the SSHD configuration file
    # Add host key into container for test
    # Put the sshd container on startup process 
    # creating link to the configuration file for 
    # the init.d script
    ebegin "Openssh installation and configuration"
    install_pkg_list SSHD
    eend 0

    ebegin "SSHD container creation"
    sed -i -e "s/container_veth/sshdiface/" ${TARGET_DIR}/etc/lxc/guest.conf
    sed -i -e "s/container_ip/${1}/" ${TARGET_DIR}/etc/lxc/guest.conf

    while true; do
        ewarn "Choose the container size. ex: 1G for one gigabyte, 500M for 500 megabytes"
        read SERVICE_SIZE
        if [[ ${SERVICE_SIZE} =~ [0-9]+G|[0-9]+M ]]; then
            break
        fi
        eerror "Invalid size"
    done

    mkdir -p ${TARGET_DIR}/${LXC_PATH}/sshd
    chroot ${TARGET_DIR} /bin/bash \
        -c "lxc-create -n sshd -t sshd -f etc/lxc/guest.conf \
        -B lvm --vgname vg --fssize ${SERVICE_SIZE}"
    chroot ${TARGET_DIR} /bin/bash \
        -c "mount /dev/mapper/vg-sshd /${LXC_PATH}/sshd"

    cp ${INSTALL_CFG_DIR}/lxc/sshd_config ${TARGET_DIR}/${LXC_PATH}/sshd/etc/ssh/
    sed -i -e "s/sshdiface/container_veth/" ${TARGET_DIR}/etc/lxc/guest.conf
    sed -i -e "s/${1}/container_ip/" ${TARGET_DIR}/etc/lxc/guest.conf
    #mkdir ${TARGET_DIR}/${LXC_PATH}/sshd/root/edit_conf
    #echo 'lxc.mount.entry = /etc /root/edit_conf none rw,noexec,bind' >> ${TARGET_DIR}/${LXC_PATH}/sshd/config

    cd ${TARGET_DIR}/etc/init.d/
    ln -s lxc ${TARGET_DIR}/etc/init.d/lxc.sshd
    cd ${INSTALL_DIR}
    chroot ${TARGET_DIR} /bin/bash \
        -c "ln -s /var/lib/lxc/sshd/config /etc/lxc/sshd.conf"
    chroot ${TARGET_DIR} /bin/bash \
        -c "rc-update add lxc.sshd default"

    echo "iptables -t nat -A PREROUTING -p tcp -i ${AVAILABLE_INTERFACES[0]} --dport 22 -j DNAT --to-destination ${1}" >> ${INSTALL_CFG_DIR}/net/firewall-rules.sh
    chroot ${TARGET_DIR} /bin/bash -c "umount /dev/mapper/vg-sshd"
    eend 0
}

configure_syslog() {
    # Configure syslog like host by default
    # TODO
    echo "in works"
    ebegin "Syslog dependencies copy into container"
    chroot ${TARGET_DIR} /bin/bash \
        -c "mount /dev/mapper/vg-syslog /${LXC_PATH}/syslog"

    cp ${TARGET_DIR}/usr/lib64/libc* ${TARGET_DIR}/${LXC_PATH}/syslog/usr/lib64/
    cp ${TARGET_DIR}/usr/lib64/libpthread* ${TARGET_DIR}/${LXC_PATH}/syslog/usr/lib64/
    cp ${TARGET_DIR}/lib/ld-linux* ${TARGET_DIR}/${LXC_PATH}/syslog/usr/lib64/
    cp ${TARGET_DIR}/usr/lib64/librt* ${TARGET_DIR}/${LXC_PATH}/syslog/usr/lib64/
    cp ${TARGET_DIR}/usr/lib64/libnsl* ${TARGET_DIR}/${LXC_PATH}/syslog/usr/lib64/
    cp ${TARGET_DIR}/usr/lib64/libdl* ${TARGET_DIR}/${LXC_PATH}/syslog/usr/lib64/
    cp ${TARGET_DIR}/usr/lib64/libuuid* ${TARGET_DIR}/${LXC_PATH}/syslog/usr/lib64/
    mv ${TARGET_DIR}/${LXC_PATH}/syslog/lib64/libwrap* ${TARGET_DIR}/${LXC_PATH}/syslog/usr/lib64/

    mkdir -p ${TARGET_DIR}/${LXC_PATH}/syslog/var/run
    echo "/usr/sbin/syslog-ng" >> ${TARGET_DIR}/${LXC_PATH}/syslog/etc/init.d/rcS
    cp ${INSTALL_CFG_DIR}/log/syslog-ng-container.conf ${TARGET_DIR}/${LXC_PATH}/syslog/etc/syslog-ng/syslog-ng.conf

    chroot ${TARGET_DIR} /bin/bash \
        -c "umount /dev/mapper/vg-syslog"
    eend 0
}

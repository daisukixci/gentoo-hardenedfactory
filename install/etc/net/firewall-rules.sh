#
# Firewall rules
#

#
# Flush all tables
#
iptables -X
iptables -F
iptables -t nat -X
iptables -t nat -F
iptables -t mangle -X
iptables -t mangle -F

#
# Define all policies to drop
#
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT DROP

#
# Initialize NAT table
#
iptables -t nat -F
iptables -t nat -X
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT
iptables -t nat -P OUTPUT ACCEPT

#
# Initialize MANGLE table
#
iptables -t mangle -F
iptables -t mangle -X
iptables -t mangle -P PREROUTING ACCEPT
iptables -t mangle -P INPUT ACCEPT
iptables -t mangle -P OUTPUT ACCEPT
iptables -t mangle -P FORWARD ACCEPT

#
# Drop all malformatted connections
#
iptables -A INPUT -p tcp ! --syn -m state --state NEW -j DROP
iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP
iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP

#
# Keep all connection which are etablished and related
#
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT


#
# Variable definition
# Determine available interface and create a variable for it
# Get DNS server from file resolv.conf
AVAILABLE_INTERFACES=`ifconfig -a | grep '^e' | cut -d: -f1 `
number=0
for i in ${AVAILABLE_INTERFACES[@]}; do
    eval IFACE${number}="${i}"
    let "number ++"
done
AVAILABLE_DNS=`cat /etc/resolv.conf | grep nameserver | cut -d " " -f2`
number=1
for i in ${AVAILABLE_DNS[@]}; do
    eval NAMESERVER_${number}="${i}"
    let "number ++"
done
LOOPBACK=127.0.0.0/8

#
# Disable ping response
# Disable broadcast response
# Drop routed from source to avoid spoofing source
# Disable icmp paquet redirection which could have a negative impact on routage table
# Drop malformous icmp
#
echo "1" > /proc/sys/net/ipv4/icmp_echo_ignore_all 
echo "1" > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts 
echo "0" > /proc/sys/net/ipv4/conf/all/accept_source_route 
for interface in /proc/sys/net/ipv4/conf/*/accept_redirects; do 
 echo "0" > ${interface} 
done 
echo "1" > /proc/sys/net/ipv4/icmp_ignore_bogus_error_responses

#
# Loopback traffic
#
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

#
# Nat all traffic from host to outside
#
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE


#!/bin/bash

GRSEC_ACL="no"
LOG_IMMUTABILITY="no"
FIREWALL="no"

gradm_config() {
    echo "GrSecurity configuration"
    gradm -P
    gradm -P admin
    gradm -P shutdown
    echo '#!/bin/bash' > /etc/local.d/grsec.start 
    echo 'gradm -E' >> /etc/local.d/grsec.start 
}

log_immutability() {
    touch /var/log/sudo.log
    find /var/log/ -type f -exec chattr +a {} \;
    chattr -a /var/log/aide/aide.report
}

security_check() {
    echo "Check effective security"
    security-check.sh
}

firewall_config() {
    /var/res/firewall-rules.sh
    /etc/init.d/iptables save
    chmod 600 ${TARGET_DIR}/var/lib/iptables/rules-save
    rc-update add iptables boot
}

test [ "${FIREWALL}" = "yes" ] && firewall_config
test [ "${LOG_IMMUTABILITY}" = "yes" ] && log_immutability
test [ "${GRSEC_ACL}" = "yes" ] && gradm_config
security_check

cp /etc/bashrc /tmp/
grep -v 'sh /var/first-start.sh' /etc/bashrc > /etc/bashrc
rm /var/res/first-start.sh

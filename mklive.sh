#!/bin/sh

# @Purpose: Generate all live OS components
# Kernel and all packages verifying binaries are not
# available. Configurate live OS. Generate ISO file, live USB, CD/DVD
#
# @Author: PIQUENOT Gaetan
# @Property: AIRBUS DEFENSE AND SPACE 
# @Licence:
# @Copyright:
source /lib/rc/sh/functions.sh

LIVE_NAME="live"
LIVE_VERSION=`uname -r`
LIVE_DIR="${FCT_DIR}/prod/live"
LIVE_SRC_DIR="${FCT_DIR}/src/linux-live"
LIVE_CFG_DIR="${CFG_DIR}/live"
LIVE_POR_DIR="${LIVE_CFG_DIR}/portage"
LIVE_OPTS="--verbose"
LIVE_CFLAGS="--arch-override=${ARCH} --kernel-config=${LIVE_SRC_DIR}/config.save --kerneldir=${LIVE_SRC_DIR} --no-menuconfig --makeopts=-j5 --color --luks --lvm"
LIVE_OFLAGS="--install --kernname=${LIVE_NAME} --bootdir=${KER_DIR} --module-prefix=${LIVE_DIR} --modprobedir=${LIVE_DIR}/etc/modprob.d" 

ISO_FILE="live.iso"
ISO_DIR="${FCT_DIR}/prod/iso"
IMG_FILE="live.squashfs"
IMG_DIR="${FCT_DIR}/prod/img"
LIVEISO_DIR="${FCT_DIR}/prod/liveiso"

MOUNTPOINT="/mnt/mountpoint"

LIVE_PKG_LIST=(	
baselayout
app-arch/tar
bash
bzip2
binutils
coreutils 
cracklib
cryptsetup 
dialog
diffutils 
eselect
eselect-postgresql
e2fsprogs 
findutils 
gawk 
glibc 
grep 
grub 
gzip 
iptables 
iputils 
kbd 
kmod
less 
lshw
lvm2
makedev	
mesa 
mkfontdir 
mkfontscale 
mpg123 
net-tools 
openrc 
parted
paxctl
pax-utils 
pinentry
dev-util/pkgconfig
portage
portage-utils
procps 
psmisc 
rsync
sed 
shadow 
syslog-ng 
util-linux 
ntp 
vim 
x11-libs/pango
)

config_live_kernel() {
    # Configuration of live kernel
    einfo "[50%] Configure live kernel"
    cd ${LIVE_SRC_DIR}
    cp ${LIVE_CFG_DIR}/config .config
    make menuconfig
    cp .config config.save
    cp .config ${LIVE_CFG_DIR}/config.save
    cd ${FCT_DIR}
}

compile_live_kernel() {
    # Compile live kernel
    einfo "[53%] Compile live kernel"
    genkernel ${LIVE_CFLAGS} ${LIVE_OFLAGS} all > ${TMP_DIR}/genkernel.log 2>&1
    if [ ${?} -ne 0 ]
    then
        cat ${TMP_DIR}/genkernel.log >> ${LOGFILE}
        clear
        echo "Something gone wrong, consult log"
        exit -1
    fi
}

configure_live_portage() {
    # Make a /etc/portage/ back up and 
    # install personnalized configuration
    einfo "[58%] Configure live portage"
    mkdir -p ${POR_BKP}
    cp /etc/portage/make.conf ${POR_BKP}
    if [ -e /etc/portage/make.profile/package.use  ]
    then
        cp /etc/portage/make.profile/package.use ${POR_BKP}
    else
        touch /etc/portage/make.profile/package.use
        cp /etc/portage/make.profile/package.use ${POR_BKP}
    fi

    test -d ${LIVE_DIR} || mkdir -p ${LIVE_DIR}
    cp ${LIVE_POR_DIR}/make.conf /etc/portage/make.conf
    cp ${LIVE_POR_DIR}/package.use /etc/portage/make.profile/package.use

    mkdir -p ${LIVE_DIR}/lib64 
    cd ${LIVE_DIR}
    test -L lib || ln -s lib64 lib
    cd ${FCT_DIR}
}

search_live_pkg() {
    # Search for already installed packages into 
    # the live directory, and install missing ones
    einfo "[63%] Search missing packages"

    for i in ${LIVE_PKG_LIST[@]}; do
        INSTALL_TEST=`qlist -I --root=${LIVE_DIR} ${i}`
        if [ -z "${INSTALL_TEST}" ]
        then
            LIVE_PKG_INLINE+="${i} "
        fi
    done
}

install_live_pkg() {
    einfo "[65%] Install live packages"
    if [ -n "${LIVE_PKG_INLINE}" ]
    then
        ROOT=${LIVE_DIR} emerge ${LIVE_OPTS} ${LIVE_PKG_INLINE} > ${TMP_DIR}/emerge.log 2>&1
        if [ ${?} -ne 0 ]
        then
            cat ${TMP_DIR}/emerge.log >> ${LOGFILE}
            clear
            echo "Something gone wrong, consult log"
            exit -1
        fi
    fi
}

restore_live_portage() {
    # Restore original portage configuration
    einfo "[66%] Restore normal portage configuration"
    cp ${POR_BKP}/make.conf /etc/portage/
    cp ${POR_BKP}/package.use /etc/portage/make.profile/
}

configure_live_filesystem() {
    # Create file system for live OS
    einfo "[67%] Configure live system"
    mkdir -p ${LIVE_DIR}/{home,media,mnt,opt,root,proc,sys,usr/portage,usr/overlay,}
    MAKEDEV -d ${LIVE_DIR}/dev console bus core cpu dri fd stderr stdout stdin urandom random zero
    MAKEDEV -xd ${LIVE_DIR}/dev sda sda1 sda2 sda3 sda4 sda5 sda6 sda7 sda8 sda9 
    MAKEDEV -xd ${LIVE_DIR}/dev tty tty1 tty2 tty3 tty4 tty5 tty6 tty7 tty8 tty9 
    MAKEDEV -xd ${LIVE_DIR}/dev loop0 loop1 loop2 loop3 loop4 loop5 loop6 loop7 loop8 loop9 

    chroot ${LIVE_DIR} /bin/bash -c "rc-update del netmount default" >/dev/null 2>&1
    chroot ${LIVE_DIR} /bin/bash -c "rc-update add syslog-ng default" >/dev/null 2>&1
    #chroot ${LIVE_DIR} /bin/bash -c "/etc/init.d/local status &>/dev/null">/dev/null 2>&1

    test -d ${LIVE_DIR}/usr/portage/profiles || cp -farp /usr/portage/profiles ${LIVE_DIR}/usr/portage/
    #test -d ${LIVE_DIR}/usr/overlay/profiles || cp -farp /usr/overlay/profiles ${LIVE_DIR}/usr/overlay/
    #test -d ${LIVE_DIR}/usr/overlay/metadata || cp -farp /usr/overlay/metadata ${LIVE_DIR}/usr/overlay/
}

configure_live_fstab() {
    # Configuration of partitions for live OS
    einfo "[68%] Configure fstab"
    test -d ${LIVE_DIR}/etc || mkdir -p ${LIVE_DIR}/etc
    cp ${LIVE_CFG_DIR}/fstab ${LIVE_DIR}/etc/
    rm -rf ${LIVE_DIR}/etc/mtab
    touch ${LIVE_DIR}/etc/mtab
    rm -rf ${LIVE_DIR}/var/lock/*
}

configure_live_misc() {
    # Configure live keyboard, console, and others local stuff
    einfo "[69%]Configure miscellaneous"
    test -d ${LIVE_DIR}/etc/conf.d || mkdir -p ${LIVE_DIR}/etc/conf.d
    sed -i -e "/^keymap=/s:us:fr-latin9:" ${LIVE_DIR}/etc/conf.d/keymaps
    sed -i -e "/^consolefont=/s:default8x16:lat9w-16:" ${LIVE_DIR}/etc/conf.d/consolefont
    echo Europe/Paris > ${LIVE_DIR}/etc/timezone
    sed -i -e "/^hostname=/s:=\"*[a-z]*\":=\"airbuslive\":" ${LIVE_DIR}/etc/conf.d/hostname
    sed -i "s/agetty 38400 tty1/agetty --noclear -n -l \/bin\/login.sh 38400 tty1/" ${LIVE_DIR}/etc/inittab
    cp ${LIVE_CFG_DIR}/login.sh ${LIVE_DIR}/bin/
    chmod u+x ${LIVE_DIR}/bin/login.sh
}

download_containers() {
    # Download containers for future installation
    # only if timestamp have been expired
    # First check timestamp (during 1 month)
    # Download the index to find containers' paths
    # Donwload all containers files
    # Prepare them for copy into live OS
    einfo "[70%] Download container"
    mkdir -p ${FCT_DIR}/res/download
    test -f ${FCT_DIR}/res/download/expiry \
        || echo "$(date +%Y%m%d)" > ${FCT_DIR}/res/download/expiry
    TIMESTAMP=`cat ${FCT_DIR}/res/download/expiry`
    if [ ${TIMESTAMP} -gt $(date +%Y%m%d) ]; then
        return 0
    fi
    date -d "today + 1 month" +%Y%m%d > ${FCT_DIR}/res/download/expiry

    wget images.linuxcontainers.org/meta/1.0/index-system \
        -O ${TMP_DIR}/container.index

    for i in 3 25 40; do
        CONTAINER_URL=$(cat ${TMP_DIR}/container.index | sed -n ${i}p | cut -d ";" -f6)
        TIME_URL=$(echo $CONTAINER_URL | cut -d/ -f7)
        CONTAINER_PATH=${CONTAINER_URL#/images}
        CONTAINER_PATH=${CONTAINER_PATH/${TIME_URL}\//}
        mkdir -p ${TMP_DIR}/${CONTAINER_PATH} 
        wget images.linuxcontainers.org/${CONTAINER_URL}/rootfs.tar.xz \
            -O ${TMP_DIR}/${CONTAINER_PATH}/rootfs.tar.xz
        wget images.linuxcontainers.org/${CONTAINER_URL}/meta.tar.xz \
            -O ${TMP_DIR}/${CONTAINER_PATH}/meta.tar.xz
        mkdir -p ${FCT_DIR}/res/download/${CONTAINER_PATH}
        mv ${TMP_DIR}/${CONTAINER_PATH}/rootfs.tar.xz ${FCT_DIR}/res/download/${CONTAINER_PATH}
        mv ${TMP_DIR}/${CONTAINER_PATH}/meta.tar.xz ${FCT_DIR}/res/download/${CONTAINER_PATH}
        echo ${TIME_URL} > ${FCT_DIR}/res/download/${CONTAINER_PATH}/build_id
        cd ${FCT_DIR}/res/download/${CONTAINER_PATH}
        tar xJf meta.tar.xz
        rm meta.tar.xz
        cd ${FCT_DIR}
    done

    rm -rf ${TMP_DIR}
}

copy_hardened() {
    # Copy hardened OS file needed for deployement
    einfo "[80%] Copy hardened files into live system"
    test -d ${LIVE_DIR}/root/hardened/pkg && rm -rf ${LIVE_DIR}/root/hardened/pkg
    test -d ${LIVE_DIR}/root/hardened/install/ && rm -rf ${LIVE_DIR}/root/hardened/install/
    mkdir -p ${LIVE_DIR}/root/hardened/install/{etc/portage,kernel,conf,}
    cp -R ${FCT_DIR}/install ${LIVE_DIR}/root/hardened/
    cp -a ${FCT_DIR}/res ${LIVE_DIR}/root/hardened/install/
    cp -R ${FCT_DIR}/conf/* ${LIVE_DIR}/root/hardened/install/conf/
    cp -R ${FCT_DIR}/etc/hard/${ARCH} ${LIVE_DIR}/root/hardened/install/etc/portage/
    cp -R ${PKG_DIR} ${LIVE_DIR}/root/hardened/
    cp -R ${KER_DIR}/*.tar.bz2 ${LIVE_DIR}/root/hardened/install/kernel
    cp -R /etc/portage ${LIVE_DIR}/etc/
    test -d ${LIVE_DIR}/usr/portage/profiles/ || mkdir -p ${LIVE_DIR}/usr/portage/profiles/
    cp -R /usr/portage/profiles/hardened ${LIVE_DIR}/usr/portage/profiles/
}

configure_iso_grub() {
    # Configuration of grub for bootable ISO generation
    einfo "[85%] Generate live ISO grub configuration file"
    test -d ${LIVEISO_DIR}/boot/grub || mkdir -p ${LIVEISO_DIR}/boot/grub
    cp ${LIVE_CFG_DIR}/grub.cfg.iso ${LIVEISO_DIR}/boot/grub/grub.cfg
    touch ${LIVEISO_DIR}/livecd
}

make_img() {
    # Make IMG file from the live directory
    einfo "[89%] Generate SQUASHFS"
    test -d ${IMG_DIR} || mkdir ${IMG_DIR}
    test -e ${IMG_DIR}/${IMG_FILE} && rm ${IMG_DIR}/${IMG_FILE}
    mkdir -p ${TMP_DIR}
    mksquashfs ${LIVE_DIR} ${IMG_DIR}/${IMG_FILE} > ${TMP_DIR}/squashfs.log
    if [ ${?} -ne 0 ]
    then
        cat ${TMP_DIR}/squashfs.log >> ${LOGFILE}
        clear
        echo "Something gone wrong, consult log"
        exit -1
    fi
    rm -rf ${TMP_DIR}
}

copy_liveiso() {
    # Copy live OS kernel and init into the 
    # ready to use folder for ISO creation
    einfo "[91%] Prepare live ISO creation"
    test -d ${LIVEISO_DIR}/boot || mkdir -p ${LIVEISO_DIR}/boot
    cp ${IMG_DIR}/${IMG_FILE} ${LIVEISO_DIR}
    cp ${KER_DIR}/kernel-${LIVE_NAME}-${ARCH}-${LIVE_VERSION} ${LIVEISO_DIR}/boot/kernel-generic
    cp ${KER_DIR}/System.map-${LIVE_NAME}-${ARCH}-${LIVE_VERSION} ${LIVEISO_DIR}/boot/System.map
    cp ${KER_DIR}/initramfs-${LIVE_NAME}-${ARCH}-${LIVE_VERSION} ${LIVEISO_DIR}/boot/initramfs-generic
}

make_iso() {
    # Make ISO file from the live ISO directory
    einfo "[95%] Make ISO"
    test -d ${ISO_DIR} || mkdir ${ISO_DIR}
    test -e ${ISO_DIR}/${ISO_FILE} && rm ${ISO_DIR}/${ISO_FILE}
    mkdir -p ${TMP_DIR}
    grub2-mkrescue -o ${ISO_DIR}/${ISO_FILE} ${LIVEISO_DIR} > ${TMP_DIR}/grub.log 2>&1
    if [ ${?} -ne 0 ]
    then
        cat ${TMP_DIR}/grub.log >> ${LOGFILE}
        clear
        echo "Something gone wrong, consult log"
        exit -1
    fi
    rm -rf ${TMP_DIR}
}

select_device() {
    # Select the device which will be used for live USB
    # Ask to insert the media
    # List all USB devices without showing their partitions
    # Select the target media
    # Get the media device node
    # Verify if the media is mounted. Unmount it in this case
    ewarn "Insert device and press ENTER"
    read -n 1 VAR_TMP
    ewarn "Please select the remove media"
    DEV_DISK=/dev/disk/by-id
    LEN=${#DEV_DISK}


    for i in ${USB_LIST}; do
        USB_DEVICE+=" ${i:LEN+1}"
    done

    select TARGET_USB in ${USB_DEVICE}; do
        break
    done
    einfo "You choose ${TARGET_USB}"

    TARGET_NODE=`udevadm info -n /dev/disk/by-id/"${TARGET_USB}" | grep ^N | cut -d" " -f2`

    mount | grep ${TARGET_NODE} > /dev/null
    if [ ${?} -eq 0 ]
    then 
        einfo "Your device will be unmounted"
        umount /dev/${TARGET_NODE}?
    else
        einfo "You device is ready"
    fi
}

format_device() {
    # Format the device and installing MBR into it
    einfo "Formating device"
    ewarn "Warning, this operation will be erase all contents of the device"
    ewarn "Do you want to continue ? Type YES to continue others response will cancelled operation"
    read -n 4 AGREEMENT
    if [ ${AGREEMENT} = "YES" ]; then
        einfo "Formating device"


        einfo "Installing MBR"


    else

        exit 1
    fi
    einfo "Your device have been formated"
}

install_liveusb() {
    # Copy kernel, initramfs into device, 
    # configure and install GRUB into it
    einfo "Live Grub configuration"

    test -d ${MOUNTPOINT}/boot/grub || mkdir -p ${MOUNTPOINT}/boot/grub
    cp -p ${IMG_DIR}/${IMG_FILE} ${MOUNTPOINT}/
    cp -p ${KER_DIR}/kernel-${LIVE_NAME}-${ARCH}-${LIVE_VERSION} ${TMP_DIR}/boot/kernel-generic
    cp -p ${KER_DIR}/initramfs-${LIVE_NAME}-${ARCH}-${LIVE_VERSION} ${TMP_DIR}/boot/initramfs-generic
    cp -p ${KER_DIR}/System.map-${LIVE_NAME}-${ARCH}-${LIVE_VERSION} ${TMP_DIR}/boot/System.map
    cp -p ${LIVE_CFG_DIR}/grub.cfg.usb ${TMP_DIR}/boot/grub/grub.cfg
    test -e ${TMP_DIR}/boot/grub/usbstick || touch ${TMP_DIR}/boot/grub/usbstick
    echo "usbstick" > ${TMP_DIR}/boot/grub/usbstick 
    cp -R -p ${TMP_DIR}/boot ${MOUNTPOINT}/
    touch ${MOUNTPOINT}/livecd


    einfo "Unmounting device"
    umount ${MOUNTPOINT}
}

insert_disk() {
    # Tell to user to  insert disk and verify it
    einfo "Please insert your empty disk and press ENTER"
    eject /dev/sr0
    read -n 1 key
    eject /dev/sr0 -t
    einfo "CD-ROM check"
}

burn_iso() {
    # Burn the iso file on CD/DVD
    einfo "Format CD/DVD"



    einfo "Burn ISO file into CD/DVD"

    cdrecord -v dev=/dev/sr0 ${ISO_DIR}/${ISO_FILE}
    eject /dev/sr0
}

###################
# DEBUG FUNCTIONS #
###################
print_live_var() {
    echo "Live name ${LIVE_NAME}"
    echo "Live version ${LIVE_VERSION}"
    echo "Live directory ${LIVE_DIR}"
    echo "Live configuration directory ${LIVE_CFG_DIR}"
    echo "Live portage configuration directory ${LIVE_POR_DIR}"
    echo "Compilation options ${LIVE_OPTS}"
    echo "Live kernel compilation flags ${LIVE_CFLAGS}"
    echo "Live kernel output flags ${LIVE_OFLAGS}"
    echo "ISO file ${ISO_FILE}"
    echo "ISO file directory ${ISO_DIR}"
    echo "IMG file ${IMG_FILE}"
    echo "IMG file directory ${ISO_FILE}"
    echo "LiveUSB directory ${LIVEISO_DIR}"
    echo "Mount point ${MOUNTPOINT}"
}

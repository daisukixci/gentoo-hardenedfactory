#!/bin/sh
 
# @Purpose: Generate all hardened OS components
# Kernel and all packages verifying binaries are not
# available
#
# @Author: PIQUENOT Gaetan
# @Property: AIRBUS DEFENSE AND SPACE 
# @Licence:
# @Copyright:

HARD_NAME="hardened-kernel-airbus"
HARD_SRC_DIR="${FCT_DIR}/src/linux-hardened"
HARD_CFG_DIR="${CFG_DIR}/hard"
HARD_POR_DIR="${HARD_CFG_DIR}/${ARCH}"
HARD_CACHE_DIR="${FCT_DIR}/cache"

HARD_OPTS="--buildpkg"
HARD_CFLAGS="--arch-override=${ARCH} --kerneldir=${HARD_SRC_DIR} --kernel-config=${HARD_SRC_DIR}/config.save --no-menuconfig --makeopts=-j5 --luks --lvm --color --no-ramdisk-modules"
HARD_OFLAGS="--minkernpackage=${KER_DIR}/${HARD_NAME}.tar.bz2"


configure_hard_kernel() {
    # Configuration of the guest kernel
    # and save the generated configuration
    # in a new file to avoid erasing older 
    # configurations
    cd ${HARD_SRC_DIR}
    cp ${HARD_CFG_DIR}/config .config
    make menuconfig
    cp .config config.save
    cp .config ${HARD_CFG_DIR}/config.save
    cd ${FCT_DIR}
}

compile_hard_kernel() {
    # Build the hardened kernel with genkernel
    genkernel ${HARD_CFLAGS} ${HARD_OFLAGS} bzImage > ${TMP_DIR}/genkernel.log 2>&1
    if [ ${?} -ne 0 ]
    then
        cat ${TMP_DIR}/genkernel.log >> ${LOGFILE}
        clear
        echo "Something gone wrong, consult log"
        exit -1
    fi
}

configure_hard_portage() {
    # Make a /etc/portage/ back up
    # Install the correct files after
    einfo "[15%]Configure hardened portage"

    mkdir -p ${POR_BKP}/make.profile
    mkdir -p /etc/portage/package.use

    cp /etc/portage/make.conf ${POR_BKP}
    cp -R /etc/portage/package.use/ ${POR_BKP}/

    if [ -e /etc/portage/make.profile/package.accept_keywords ]
    then
        cp /etc/portage/make.profile/package.accept_keywords ${POR_BKP}/make.profile
    fi
    if [ -e /etc/portage/make.profile/package.use ]
    then
        cp /etc/portage/make.profile/package.use ${POR_BKP}/make.profile
    fi
    if [ -e /etc/portage/make.profile/package.mask ]
    then
        cp /etc/portage/make.profile/package.mask ${POR_BKP}/make.profile
    fi
    touch ${POR_BKP}/make.profile/package.accept_keywords
    touch ${POR_BKP}/make.profile/package.use
    touch ${POR_BKP}/make.profile/package.mask

    cp ${HARD_POR_DIR}/make.conf /etc/portage/
    cp ${HARD_POR_DIR}/make.profile/package.use /etc/portage/make.profile/
    cp ${HARD_POR_DIR}/make.profile/package.accept_keywords /etc/portage/make.profile/
    cp ${HARD_POR_DIR}/make.profile/package.mask /etc/portage/make.profile/
    cp ${HARD_POR_DIR}/package.use/* /etc/portage/package.use/

    test -d ${PKG_DIR} || mkdir ${PKG_DIR}
    test -d ${HARD_CACHE_DIR} || mkdir ${HARD_CACHE_DIR}

    mkdir -p ${HARD_CACHE_DIR}/lib64
    cd ${HARD_CACHE_DIR}
    test -L lib || ln -s lib64 lib
    cd ${FCT_DIR}
}

merge_hard_list() {
    # Put both packages list in one inline var
    # if there are not already compiled
    # to compile them with emerge
    einfo "[17%] Search missing packages. It will take a while..."

    for i in ${BASE_PKG_LIST[@]}; do
        einfo "Search for ${i}"
        ROOT=${HARD_CACHE_DIR} PKGDIR=${PKG_DIR} emerge -Kp ${i} > /dev/null 2>&1
        if [ ${?} -eq 1 ]
        then
            einfo "${i} will be compiled"
            PKG_INLINE+="${i} "
        fi
    done

    einfo "[25%] Search missing packages. It will take a while..."
    for service in ${AVAILABLE_SERVICES[@]}; do
        if [ "${!service}" = "on" ]
        then
            einfo "Search for ${i}"
            PKG_LIST_NAME="${service}_PKG"
            eval PKG_LIST=\${$PKG_LIST_NAME[@]}
            for i in ${PKG_LIST[@]}; do
                ROOT=${HARD_CACHE_DIR} PKGDIR=${PKG_DIR} emerge -Kp ${i} > /dev/null 2>&1
                if [ ${?} -eq 1 ]
                then
                    einfo "${i} will be compiled"
                    PKG_INLINE+="${i} "
                fi
            done
        fi
    done
    einfo "[30%] Search missing packages"
}

make_hard_pkg() {
    # Compile packages and export them
    # in $PKGDIR into tar.bz2
    einfo "[31%] Compile hardened packages. It can take a while ..."
    if [ -n "${PKG_INLINE}" ]
    then
        einfo "Packages following will be compiled: ${PKG_INLINE}"
        PKGDIR=${PKG_DIR} ROOT=${HARD_CACHE_DIR} emerge ${HARD_OPTS} ${PKG_INLINE} > ${TMP_DIR}/emerge.log 2>&1
        if [ ${?} -ne 0 ]
        then
            cat ${TMP_DIR}/emerge.log >> ${LOGFILE}
            clear
            echo "Something gone wrong, consult log"
            exit -1
        fi
    fi
    einfo "[45%] Compile hardened packages"
}

restore_hard_portage() {
    # Restore original portage configuration
    einfo "[47%] Restore normal portage configuration"
    cp ${POR_BKP}/make.conf /etc/portage/
    cp ${POR_BKP}/make.profile/package.accept_keywords /etc/portage/make.profile/
    cp ${POR_BKP}/make.profile/package.mask /etc/portage/make.profile/
    cp ${POR_BKP}/make.profile/package.use /etc/portage/make.profile/
    rm -rf /etc/portage/package.use
    cp -R ${POR_BKP}/package.use /etc/portage/
}

###################
# DEBUG FUNCTIONS #
###################
print_hard_var() {
    echo "Hardened kernel name ${HARD_NAME}"
    echo "Hardened source directory ${HARD_SRC_DIR}"
    echo "Hardened configuration directory ${HARD_CFG_DIR}"
    echo "Hardened portage configuration directory ${HARD_POR_DIR}"
    echo "Hardened packages directory ${PKG_DIR}"
    echo "Hardened compilation options ${HARD_OPTS}"
    echo "Hardened compilation flags ${HARD_CFLAGS}"
    echo "Hardened output flags ${HARD_OFLAGS}"
}

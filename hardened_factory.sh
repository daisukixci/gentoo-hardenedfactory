#!/bin/sh

# @Purpose: Generate a complet hardened OS and create ISO or SQUASHFS 
# file to create a bootable media. This script generate a menu to 
# personalize the futur OS
#
# @Author: PIQUENOT Gaetan
# @Property: AIRBUS DEFENSE AND SPACE 
# @Licence:
# @Copyright:

# Import enhanced print functions
source /lib/rc/sh/functions.sh
test -f "./conf/services.cfg" || eend "Services configuration file is missing" || exit 1
source ./conf/services.cfg
test -f "./conf/hardened_factory.cfg" || eend "Factory configuration file is missing" || exit 1
source ./conf/hardened_factory.cfg

FACTORY_CFG="./conf/hardened_factory.cfg"
SRV_CFG="./conf/services.cfg"

FCT_DIR=`pwd`
CFG_DIR="${FCT_DIR}/etc"
KER_DIR="${FCT_DIR}/prod/kernel"
TMP_DIR="${FCT_DIR}/tmp"
BKP_DIR="${TMP_DIR}/bkp"
POR_BKP="${BKP_DIR}/portage"
PKG_DIR="${FCT_DIR}/prod/pkg"

DIALOG_DIM=`dialog --no-shadow --print-maxsize 2>&1 | cut -d " " -f2,3 | sed -e "s/,//g"`
DIALOG_MENU_HEIGHT="25"
BACKUP_HARD="no"
BACKUP_LIVE="no"
OVERWRITE_KERNEL="no"

mkdir -p ${FCT_DIR}/log
LOGFILE="${FCT_DIR}/log/hf.$(date +%d%m%y%H%M)"
touch ${LOGFILE}

source ./mkhard.sh
source ./mklive.sh

trap clean_exit SIGTERM SIGINT

menu() {
    # Check availability of dialog utility
    which dialog &> /dev/null
    [ $? -ne 0 ]  && echo "Dialog utility is not available, Install it" && exit 1
    export DIALOGRC="./conf/menu.cfg"
    
    
    # Calls delete_menu_tmp function on exit
    trap 'delete_menu_tmp'  EXIT     
    dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "" \
        --textbox ./conf/issue.ascii 30 120

    while :
    do
        # Dialog utility to display options list
        # This is the main menu. Nothing here is tricky. 
        # Each menu have his own dialog interface to change
        # parameters and put in a temporary file by redirect stderr 
        # to the file.
        dialog --help-button --clear --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "MAIN MENU" \
            --menu "Use [UP/DOWN] key to move" ${DIALOG_DIM} ${DIALOG_MENU_HEIGHT}\
            "Source"            "Select your hardened version sources" \
            "Kernel"            "Configure kernel for your hardware specification" \
            "Configuration"     "Configurate services for hardened factory to your needs" \
            "Display"           "Display actual configuration" \
            "Iso"               "Generate the live ISO" \
            "Usb"               "Generate the live USB" \
            "CD/DVD"            "Generate the live CD/DVD" \
            "Exit"              "To exit" 2> menuchoices.main

        retopt=${?}
        CHOICE=`cat menuchoices.main`

        case ${retopt} in
            0) 
                case ${CHOICE} in
                    Source)
                        choose_source
                        ;;
                    Kernel)
                        configure_hard_kernel
                        config_live_kernel
                        ;;
                    Configuration)
                        while : 
                        do
                            dialog --help-button --backtitle "AIRBUS HARDENED FACTORY V0.1" \
                                --menu "Options menu:" ${DIALOG_DIM} ${DIALOG_MENU_HEIGHT}\
                                "Architecture"  "Choose your architecture" \
                                "Cipher"        "Choose your cipher algorithm" \
                                "Editor"        "Choose your editor" \
                                "KeySize"       "Choose your keysize" \
                                "Network"       "Choose your network support" \
                                "Partition"     "Choose your partitions size" \
                                "Security"      "Choose your security" \
                                "Service"       "Choose your applications" 2> menuchoices.options
                            retopt=${?}
                            CHOICE=`cat menuchoices.options`
                            case ${retopt} in
                                0)
                                    case ${CHOICE} in
                                        Architecture)
                                            case "${ARCH}" in
                                                x86_64)
                                                    MENU_X86_64="on"
                                                    MENU_X86="off"
                                                    ;;
                                                x86)
                                                    MENU_X86_64="off"
                                                    MENU_X86="on"
                                                    ;;
                                            esac
                                            dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Architecture selection" \
                                                --radiolist "Choose your options:" ${DIALOG_DIM} ${DIALOG_MENU_HEIGHT} \
                                                x86_64  "for computer with ram >2G" ${MENU_X86_64} \
                                                x86     "for old computer"          ${MENU_X86} 2> menuchoices.arch
                                            CHOICE=`cat menuchoices.arch`
                                            if [ -z ${CHOICE} ]; then
                                                sed -i -e "s/ARCH=\".*\"/ARCH=\"x86_64\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/ARCH=\".*\"/ARCH=\"${CHOICE}\"/g" ${FACTORY_CFG}
                                            fi
                                            import_config
                                            ;;
                                        Cipher)
                                            case "${CIPHER}" in
                                                no)
                                                    MENU_NO="on"
                                                    MENU_AES="off"
                                                    MENU_BLOWFISH="off"
                                                    MENU_SERPENT="off"
                                                    MENU_TWOFISH="off"
                                                    ;;
                                                aes)
                                                    MENU_NO="off"
                                                    MENU_AES="on"
                                                    MENU_BLOWFISH="off"
                                                    MENU_SERPENT="off"
                                                    MENU_TWOFISH="off"
                                                    ;;
                                                blowfish)
                                                    MENU_NO="off"
                                                    MENU_AES="off"
                                                    MENU_BLOWFISH="on"
                                                    MENU_SERPENT="off"
                                                    MENU_TWOFISH="off"
                                                    ;;
                                                serpent)
                                                    MENU_NO="off"
                                                    MENU_AES="off"
                                                    MENU_BLOWFISH="off"
                                                    MENU_SERPENT="on"
                                                    MENU_TWOFISH="off"
                                                    ;;
                                                twofish)
                                                    MENU_NO="off"
                                                    MENU_AES="off"
                                                    MENU_BLOWFISH="off"
                                                    MENU_SERPENT="off"
                                                    MENU_TWOFISH="on"
                                                    ;;
                                                *)
                                                    ;;
                                            esac
                                            dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Cipher selection" \
                                                --radiolist "Choose your options:" ${DIALOG_DIM} ${DIALOG_MENU_HEIGHT} \
                                                no          "No encryption"                 ${MENU_NO} \
                                                aes         "american standard"             ${MENU_AES} \
                                                blowfish    "used in openssh and gpg"       ${MENU_BLOWFISH} \
                                                serpent     "second place in AES challenge" ${MENU_SERPENT} \
                                                twofish     "blowfish successor"            ${MENU_TWOFISH} 2> menuchoices.cipher
                                            CHOICE=`cat menuchoices.cipher`
                                            if [ -z ${CHOICE} ]; then
                                                sed -i -e "s/CIPHER=\".*\"/CIPHER=\"no\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/CIPHER=\".*\"/CIPHER=\"${CHOICE}\"/g" ${FACTORY_CFG}
                                            fi
                                            import_config
                                            ;;
                                        Editor)
                                            dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Editor choice" \
                                                --checklist "Choose your options" ${DIALOG_DIM} ${DIALOG_MENU_HEIGHT} \
                                                emacs   "EMACS" ${EMACS} \
                                                nano    "NANO"  ${NANO} \
                                                vim     "VIM"   ${VIM} 2> menuchoices.edit
                                            if grep -q emacs menuchoices.edit;
                                            then
                                                sed -i -e "s/EMACS=\".*\"/EMACS=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/EMACS=\".*\"/EMACS=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q nano menuchoices.edit;
                                            then
                                                sed -i -e "s/NANO=\".*\"/NANO=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/NANO=\".*\"/NANO=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q vim menuchoices.edit;
                                            then
                                                sed -i -e "s/VIM=\".*\"/VIM=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/VIM=\".*\"/VIM=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            import_config
                                            ;;
                                        KeySize)
                                            case "${KEY_SIZE}" in
                                                128)
                                                    MENU_128="on"
                                                    MENU_192="off"
                                                    MENU_256="off"
                                                    MENU_448="off"
                                                    ;;
                                                192)
                                                    MENU_128="off"
                                                    MENU_192="on"
                                                    MENU_256="off"
                                                    MENU_448="off"
                                                    ;;
                                                256)
                                                    MENU_128="off"
                                                    MENU_192="off"
                                                    MENU_256="on"
                                                    MENU_448="off"
                                                    ;;
                                                448)
                                                    MENU_128="off"
                                                    MENU_192="off"
                                                    MENU_256="off"
                                                    MENU_448="on"
                                                    ;;
                                                *)
                                                    ;;
                                            esac
                                            case "${CIPHER}" in
                                                no)
                                                    dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Cipher key size" \
                                                        --msgbox "You choose no encryption so you don't need to \
                                                        choose a key size" ${DIALOG_DIM}
                                                    ;;
                                                aes|twofish|serpent)
                                                    if [ "${KEY_SIZE}" -eq 448 ]; then
                                                        KEY_SIZE=256
                                                        MENU_128="off"
                                                        MENU_192="off"
                                                        MENU_256="on"
                                                        MENU_448="off"
                                                    fi
                                                    dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Cipher key size" \
                                                        --radiolist "Choose your options:" ${DIALOG_DIM} ${DIALOG_MENU_HEIGHT} \
                                                        128     "not recommended"   ${MENU_128} \
                                                        192     "not recommended"   ${MENU_192} \
                                                        256     "recommended"       ${MENU_256} 2> menuchoices.key
                                                    ;;
                                                blowfish)
                                                    dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Cipher key size" \
                                                        --radiolist "Choose your options:" ${DIALOG_DIM} ${DIALOG_MENU_HEIGHT} \
                                                        128     "not recommended"   ${MENU_128} \
                                                        192     "not recommended"   ${MENU_192} \
                                                        256     "not recommended"   ${MENU_256} \
                                                        448     "recommended"       ${MENU_448} 2> menuchoices.key
                                                    ;;
                                            esac
                                            CHOICE=`cat menuchoices.key`
                                            if [ -z ${CHOICE} ]; then

                                                sed -i -e "s/KEY_SIZE=\".*\"/KEY_SIZE=\"256\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/KEY_SIZE=\".*\"/KEY_SIZE=\"${CHOICE}\"/g" ${FACTORY_CFG}
                                            fi
                                            import_config
                                            ;;
                                        Network)
                                            dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Enable IPV6 support" \
                                                --radiolist "Choose your options" ${DIALOG_DIM} ${DIALOG_MENU_HEIGHT} \
                                                on  "Activate IPV6"                                     on \
                                                off "Disable IPV6. More efficient if disable in kernel" off 2> menuchoices.net
                                            CHOICE=`cat menuchoices.net`
                                            if [ -z ${CHOICE} ]
                                            then
                                                sed -i -e "s/IPV6=\".*\"/IPV6=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/IPV6=\".*\"/IPV6=\"${CHOICE}\"/g" ${FACTORY_CFG}
                                            fi
                                            import_config
                                            ;;
                                        Partition)
                                            while : 
                                            do
                                                dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" \
                                                    --menu "Partitions menu:" ${DIALOG_DIM} ${DIALOG_MENU_HEIGHT}\
                                                    "Root"  "Choose root size" \
                                                    "Etc"   "Choose etc size" \
                                                    "Usr"   "Choose usr size" \
                                                    "Tmp"   "Choose tmp size" \
                                                    "Dev"   "Choose dev size" \
                                                    "Var"   "Choose var size" 2> menuchoices.parts
                                                retopt=${?}
                                                CHOICE=`cat menuchoices.parts`
                                                case ${retopt} in
                                                    0)
                                                        case ${CHOICE} in
                                                            Root)
                                                                dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Partition size" \
                                                                    --inputbox "Root size" ${DIALOG_DIM} "${SIZE_ROOT}" 2> menuchoices.root
                                                                SIZE_ROOT=`cat menuchoices.root`
                                                                if [ -z "${SIZE_ROOT}" ]; then
                                                                    SIZE_ROOT="1G"
                                                                fi
                                                                sed -i -e "s/SIZE_ROOT=\".*\"/SIZE_ROOT=\"${SIZE_ROOT}\"/" ${FACTORY_CFG}
                                                                ;;
                                                            Etc)
                                                                dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Partition size" \
                                                                    --inputbox "Etc size" ${DIALOG_DIM} "${SIZE_ETC}" 2> menuchoices.etc
                                                                SIZE_ETC=`cat menuchoices.etc`
                                                                if [ -z "${SIZE_ETC}" ]; then
                                                                    SIZE_ETC=1G
                                                                fi
                                                                sed -i -e "s/SIZE_ETC=\".*\"/SIZE_ETC=\"${SIZE_ETC}\"/" ${FACTORY_CFG}
                                                                ;;
                                                            Usr)
                                                                dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Partition size" \
                                                                    --inputbox "Usr size" ${DIALOG_DIM} "${SIZE_USR}" 2> menuchoices.usr
                                                                SIZE_USR=`cat menuchoices.usr`
                                                                if [ -z "${SIZE_USR}" ]; then
                                                                    SIZE_USR=1G
                                                                fi
                                                                sed -i -e "s/SIZE_USR=\".*\"/SIZE_USR=\"${SIZE_USR}\"/" ${FACTORY_CFG}
                                                                ;;
                                                            Tmp)
                                                                dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Partition size" \
                                                                    --inputbox "Tmp size" ${DIALOG_DIM} "${SIZE_TMP}" 2> menuchoices.tmp
                                                                SIZE_TMP=`cat menuchoices.tmp`
                                                                if [ -z "${SIZE_TMP}" ]; then
                                                                    SIZE_TMP=1G
                                                                fi
                                                                sed -i -e "s/SIZE_TMP=\".*\"/SIZE_TMP=\"${SIZE_TMP}\"/" ${FACTORY_CFG}
                                                                ;;
                                                            Dev)
                                                                dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Partition size" \
                                                                    --inputbox "Dev size" ${DIALOG_DIM} "${SIZE_DEV}" 2> menuchoices.dev
                                                                SIZE_DEV=`cat menuchoices.dev`
                                                                if [ -z "${SIZE_DEV}" ]; then
                                                                    SIZE_DEV=1G
                                                                fi
                                                                sed -i -e "s/SIZE_DEV=\".*\"/SIZE_DEV=\"${SIZE_DEV}\"/" ${FACTORY_CFG}
                                                                ;;
                                                            Var)
                                                                dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Partition size" \
                                                                    --inputbox "Var size" ${DIALOG_DIM} "${SIZE_VAR}" 2> menuchoices.var
                                                                SIZE_VAR=`cat menuchoices.var`
                                                                if [ -z "${SIZE_VAR}" ]; then
                                                                    SIZE_VAR=1G
                                                                fi
                                                                sed -i -e "s/SIZE_VAR=\".*\"/SIZE_VAR=\"${SIZE_VAR}\"/" ${FACTORY_CFG}
                                                                ;;
                                                            *)
                                                                break
                                                                ;;
                                                        esac
                                                        ;;
                                                    1)
                                                        clear
                                                        break
                                                        ;;
                                                    *)
                                                        clear
                                                        echo "Something gone wrong"
                                                        break
                                                        ;;
                                                esac
                                                import_config
                                            done
                                            ;;
                                        Security)
                                            dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Security choice" \
                                                --checklist "Choose your options:"  ${DIALOG_DIM} ${DIALOG_MENU_HEIGHT} \
                                                "delete root"       "delete root privileges"                ${LESS_ROOT} \
                                                "firewall"          "configure netfilter default policy"    ${FIREWALL} \
                                                "hids"              "verify system integrity"               ${HIDS} \
                                                "log imuability"    "avoid log suppression, only append"    ${LOG_IMU} \
                                                "modules"           "accept modules utilisation"            ${MODULES} \
                                                "paxctl"            "add pax control daemon"                ${PAXCTL} \
                                                "rbac"              "affinate right control with GrSec"     ${RBAC} \
                                                "rootkit"           "add a rootkit detector"                ${ROOTKIT} 2> menuchoices.sec
                                            if grep -q "rbac" menuchoices.sec;
                                            then
                                                sed -i -e "s/RBAC=\".*\"/RBAC=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/RBAC=\".*\"/RBAC=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q "delete root" menuchoices.sec;
                                            then
                                                sed -i -e "s/LESS_ROOT=\".*\"/LESS_ROOT=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/LESS_ROOT=\".*\"/LESS_ROOT=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q firewall menuchoices.sec;
                                            then
                                                sed -i -e "s/FIREWALL=\".*\"/FIREWALL=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/FIREWALL=\".*\"/FIREWALL=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q hids menuchoices.sec;
                                            then
                                                sed -i -e "s/HIDS=\".*\"/HIDS=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/HIDS=\".*\"/HIDS=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q "log imuability" menuchoices.sec;
                                            then
                                                sed -i -e "s/LOG_IMU=\".*\"/LOG_IMU=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/LOG_IMU=\".*\"/LOG_IMU=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q "modules" menuchoices.sec;
                                            then
                                                sed -i -e "s/MODULES=\".*\"/MODULES=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/MODULES=\".*\"/MODULES=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q "paxctl" menuchoices.sec;
                                            then
                                                sed -i -e "s/PAXCTL=\".*\"/PAXCTL=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/PAXCTL=\".*\"/PAXCTL=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q "rootkit" menuchoices.sec;
                                            then
                                                sed -i -e "s/ROOTKIT=\".*\"/ROOTKIT=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/ROOTKIT=\".*\"/ROOTKIT=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            import_config
                                            ;;
                                        Service)
                                            dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Service choice" \
                                                --checklist "Choose your options:"  ${DIALOG_DIM} ${DIALOG_MENU_HEIGHT} \
                                                apache  "an eprouved web server"    ${APACHE} \
                                                centos  "a complete centos"         ${CENTOS} \
                                                gentoo  "a complete gentoo"         ${GENTOO} \
                                                httpd   "minimal web server"        ${HTTPD} \
                                                nginx   "a light webserver"         ${NGINX} \
                                                ntp     "synchronize time"          ${NTP} \
                                                samba   "a file share server"       ${SAMBA} \
                                                sshd    "remote administration"     ${SSHD} \
                                                syslog  "logging system"            ${SYSLOG} \
                                                ubuntu  "a complete ubuntu"         ${UBUNTU} 2> menuchoices.srv
                                            if grep -q apache menuchoices.srv;
                                            then
                                                sed -i -e "s/APACHE=\".*\"/APACHE=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/APACHE=\".*\"/APACHE=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q centos menuchoices.srv;
                                            then
                                                sed -i -e "s/CENTOS=\".*\"/CENTOS=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/CENTOS=\".*\"/CENTOS=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q gentoo menuchoices.srv;
                                            then
                                                sed -i -e "s/GENTOO=\".*\"/GENTOO=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/GENTOO=\".*\"/GENTOO=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q httpd menuchoices.srv;
                                            then
                                                sed -i -e "s/HTTPD=\".*\"/HTTPD=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/HTTPD=\".*\"/HTTPD=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q ntp menuchoices.srv;
                                            then
                                                sed -i -e "s/NTP=\".*\"/NTP=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/NTP=\".*\"/NTP=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q nginx menuchoices.srv;
                                            then
                                                sed -i -e "s/NGINX=\".*\"/NGINX=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/NGINX=\".*\"/NGINX=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q samba menuchoices.srv;
                                            then
                                                sed -i -e "s/SAMBA=\".*\"/SAMBA=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/SAMBA=\".*\"/SAMBA=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q sshd menuchoices.srv;
                                            then
                                                sed -i -e "s/SSHD=\".*\"/SSHD=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/SSHD=\".*\"/SSHD=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q syslog menuchoices.srv;
                                            then
                                                sed -i -e "s/SYSLOG=\".*\"/SYSLOG=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/SYSLOG=\".*\"/SYSLOG=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            if grep -q ubuntu menuchoices.srv;
                                            then
                                                sed -i -e "s/UBUNTU=\".*\"/UBUNTU=\"on\"/g" ${FACTORY_CFG}
                                            else
                                                sed -i -e "s/UBUNTU=\".*\"/UBUNTU=\"off\"/g" ${FACTORY_CFG}
                                            fi
                                            import_config
                                            ;;
                                        *)
                                            clear
                                            echo "Impossible to go in suboptions menu"
                                            exit
                                            ;;
                                    esac
                                    ;;
                                1)
                                    clear
                                    break
                                    ;;
                                2)
                                    clear
                                    HELP=`echo $CHOICE | cut -d " " -f2`
                                    case $HELP in
                                        Architecture)
                                            dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Help" \
                                                --msgbox "\nYou can choose the hardware architecture. \
                                                At this time, only amd64 (x86_64) is available." ${DIALOG_DIM}
                                            ;;
                                        Cipher)
                                            dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Help" \
                                                --msgbox "\nChoose your cipher algorithm between the \
                                                most used" ${DIALOG_DIM}
                                            ;;
                                        Editor)
                                            dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Help" \
                                                --msgbox "\nChoose your editors among followings:\
                                                \n-Emacs\n-Nano\n-Vim" ${DIALOG_DIM}
                                            ;;
                                        Keysize)
                                            dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Help" \
                                                --msgbox "\nChoose your cipher key size according to the \
                                                algorithm you chose" ${DIALOG_DIM}
                                            ;;
                                        Network)
                                            dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Help" \
                                                --msgbox "\nAdd the IPV6 support or disable it by a sysctl \
                                                configuration.\n If you really don't want IPV6 support, we advise you to \
                                                disable it into the kernel configuration." ${DIALOG_DIM}
                                            ;;
                                        Partition)
                                            dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Help" \
                                                --msgbox "\nChoose partitions size according to services \
                                                you want to activate" ${DIALOG_DIM}
                                            ;;
                                        Security)
                                            dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Help" \
                                                --msgbox "\nChoose additional security layers" ${DIALOG_DIM}
                                            ;;
                                        Service)
                                            dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Help" \
                                                --msgbox "\nChoose services you want which will be \
                                                deploy into containers among followings:\
                                                \n-Apache\
                                                \n-CentOS\
                                                \n-Gentoo\
                                                \n-Http\
                                                \n-Nginx\
                                                \n-Ntp\
                                                \n-Samba\
                                                \n-Sshd\
                                                \n-Syslog\
                                                \n-Ubuntu" ${DIALOG_DIM}
                                            ;;
                                    esac
                                    ;;
                                *)
                                    clear
                                    echo "Something gone wrong"
                                    break
                                    ;;
                            esac
                        done
                        ;;
                    Display)
                        dialog --textbox ${FACTORY_CFG} 80 100
                        ;;
                    Iso)
                        dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "ISO name" \
                            --inputbox "Enter your iso name, type enter for live.iso" \
                            ${DIALOG_DIM} "live.iso" 2> menuchoices.iso
                        ISO_FILE=`cat menuchoices.iso`
                        if [ -z ${ISO_FILE} ]; then
                            exit 
                        fi
                        clear
                        log_msg ${LOGFILE} INFO "Live ISO creation\n"
                        import_config
                        generate_hardened_kernel \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Hardened kernel compilation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        generate_hardened_packages \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Hardened packages compilation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        generate_live_kernel \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Live kernel compilation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        generate_live_packages \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Live packages compilation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        generate_live_system \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Live system configuration" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        make_img \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Image generation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        create_liveiso \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Live ISO creation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        dialog  --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "ISO name" \
                            --msgbox "ISO is ready" ${DIALOG_DIM}
                        ;;
                    Usb)
                        log_msg ${LOGFILE} INFO "Live USB creation\n"
                        import_config
                        generate_hardened_kernel \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Hardened kernel compilation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        generate_hardened_packages \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Hardened packages compilation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        generate_live_kernel \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Live kernel compilation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        generate_live_packages \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Live packages compilation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        generate_live_system \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Live system configuration" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        make_img \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Image generation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        create_liveusb
                        dialog  --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "ISO name" \
                            --msgbox "Live USB is ready"
                        ;;
                    CD/DVD)
                        log_msg ${LOGFILE} INFO "Live CD/DVD creation\n"
                        import_config
                        generate_hardened_kernel \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Hardened kernel compilation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        generate_hardened_packages \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Hardened packages compilation" ${DIALOG_DIM}
                        generate_live_kernel \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Live kernel compilation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        generate_live_packages \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Live packages compilation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        generate_live_system \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Live system configuration" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        make_img \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Image generation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        create_liveiso \
                            | dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --progressbox "Live ISO creation" ${DIALOG_DIM}
                        test ${PIPESTATUS[0]} -ne 0 && exit ${PIPESTATUS[0]}
                        create_livecd
                        dialog  --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Generation" \
                            --msgbox "Live CD/DVD is ready" ${DIALOG_DIM}
                        ;;
                    Exit)       
                        clear; 
                        exit 0
                        ;;
                esac
                ;;
            2)
                clear
                HELP=`echo $CHOICE | cut -d " " -f2`
                case $HELP in
                    Kernel)
                        dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Help" \
                            --msgbox "\nYou can adapt the futur hardened OS kernel to \
                            support your hardware. KEEP SECURITY OPTIONS ACTIVATED" ${DIALOG_DIM}
                        ;;
                    Configuration)
                        dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Help" \
                            --msgbox "\nYou can adapt the futur hardened OS to your \
                            needs and it's in this menu you can do it" ${DIALOG_DIM}
                        ;;
                    Display)
                        dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Help" \
                            --msgbox "\nDisplay the configuration to verify if all is ok \
                            according to your need" ${DIALOG_DIM}
                        ;;
                    Iso)
                        dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Help" \
                            --msgbox "\nGenerate a bootable ISO file according to install \
                            it into virtual machine" ${DIALOG_DIM}
                        ;;
                    Usb)
                        dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Help" \
                            --msgbox "\nGenerate a hardened OS and put it into a live USB \
                            to be ready to install" ${DIALOG_DIM}
                        ;;
                    CD/DVD)
                        dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Help" \
                            --msgbox "\nAfter generate the ISO file directly burn it \
                            into a CD or DVD" ${DIALOG_DIM}
                        ;;
                    Exit)
                        ;;
                esac
                ;;
            *)
                clear
                exit 
                ;;
        esac
    done
}

log_msg() {
    # Log $3 with the level $2 into $1
    printf "${2}: ${3}" >> ${1}
}

end_msg() {
    # Print ok or fail into LOGFILE 
    # according to the value passed 
    if [ ${1} -eq 0 ]; then
        printf "\t\t[OK]\n" >> ${LOGFILE}
        return 0
    else
        printf "\t\t[FAIL]\n" >> ${LOGFILE}
        return 1
    fi
}

import_config() {
    # Import the hardened factory configuration
    # file. Do this each time you modify the configuration
    # to have a configuration up to date
    source ${FACTORY_CFG}
}

clean_tmp() {
    # Clean temporary files destroying 
    # the temporary folder
    log_msg ${LOGFILE} INFO "Cleaning temporary files"
    test -d ${TMP_DIR} && rm -rf ${TMP_DIR}
    end_msg 0
}

delete_menu_tmp() {
    # Delete all temporary files used to 
    # know your choices into menus
    rm -f menuchoices.*
}

clean_exit() {
    # Clean exit according step you are
    # in OS generation.
    clear
    test "${BACKUP_HARD}" = "yes" && restore_hard_portage
    test "${BACKUP_LIVE}" = "yes" && restore_live_portage
    clean_tmp
    delete_menu_tmp
    echo "Clean exit"
    exit -1
}

choose_source() {
    # Create a menu to choose version of Gentoo hardened source 
    # you want to use. After you choose, create the correct symbolic link
    # to use the correct version
    AVAILABLE_SOURCES=`ls -l ${FCT_DIR}/src | grep "^d" | awk -F"linux-" '{print "linux-"$NF}' | grep hardened`
    if [ -z ${AVAILABLE_SOURCES} ]; then
        dialog  --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "ERROR" \
            --msgbox "No hardened sources are available" ${DIALOG_DIM}
        exit 0
    fi

    mkdir -p ${TMP_DIR}
    for iter in ${AVAILABLE_SOURCES[@]}; do
        echo "${iter} this?" >> ${TMP_DIR}/sourcechoice
    done
    dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Source selection" \
        --menu "Choose the source version you want" ${DIALOG_DIM} ${DIALOG_MENU_HEIGHT} \
        `cat ${TMP_DIR}/sourcechoice` 2> menuchoices.source 
    SOURCE_CHOICE=`cat menuchoices.source`
    if [ "${KERNEL_SOURCE}" = "${SOURCE_CHOICE}" ]; then
        OVERWRITE_KERNEL="no"
    else
        OVERWRITE_KERNEL="yes"
        sed -i -e "s/KERNEL_SOURCE=\".*\"/KERNEL_SOURCE=\"${SOURCE_CHOICE}\"/g" ${FACTORY_CFG}
        cd ${FCT_DIR}/src
        ln -snf ${SOURCE_CHOICE} linux-hardened
        cd ${FCT_DIR}
    fi
    clean_tmp
}

hardened_os_generation() {
    generate_hardened_kernel 
    generate_hardened_packages
    generate_live_kernel
    generate_live_packages
    generate_live_system
    make_img
}

generate_hardened_kernel() {
    # Compile hardened kernel if no kernel are available
    # and you didn't change the source version
    test -d ${TMP_DIR} || mkdir -p ${TMP_DIR}
    test -d ${KER_DIR} || mkdir -p ${KER_DIR}

    einfo "[ 3%] Compile hardened kernel"
    if [ ! -f ${KER_DIR}/${HARD_NAME}.tar.bz2 ] || [ "${OVERWRITE_KERNEL}" = "yes" ]
    then
        log_msg ${LOGFILE} INFO "Hardened kernel compilation"
        compile_hard_kernel
    else
        log_msg ${LOGFILE} INFO "Hardened kernel compilation bypassed"
        einfo "[10%] Hardened kernel compilation bypassed"
    fi
    end_msg 0
    clean_tmp
}

generate_hardened_packages() {
    # Configure portage to compile hardened packages
    # after check if already existing version are available
    # and compile the missing ones. At the end, restore the 
    # Portage configuration
    log_msg ${LOGFILE} INFO "Hardened Portage configuration"
    configure_hard_portage 
    BACKUP_HARD="yes"
    end_msg 0

    log_msg ${LOGFILE} INFO "Searching packages needed to be compiled"
    merge_hard_list
    end_msg 0
    log_msg ${LOGFILE} INFO "Packages which will be compiled: ${PKG_INLINE}\n" 

    log_msg ${LOGFILE} INFO "Packages compilation"
    make_hard_pkg
    end_msg 0

    log_msg ${LOGFILE} INFO "Hardened Portage restauration"
    restore_hard_portage
    BACKUP_HARD="no"
    end_msg 0

    clean_tmp
}

generate_live_kernel() {
    # Config and compile live kernel if
    # there is not available
    test -d ${TMP_DIR} || mkdir -p ${TMP_DIR}
    if [ -e ${KER_DIR}/kernel-${LIVE_NAME}-${ARCH}-${LIVE_VERSION} ]
    then
        log_msg ${LOGFILE} INFO "Live kernel compilation bypassed"
    else
        log_msg ${LOGFILE} INFO "Live kernel compilation"
        compile_live_kernel
    fi
    end_msg 0
    clean_tmp
}

generate_live_packages() {
    # Configure portage to compile live packages
    # after check if already existing version are available
    # and compile the missing ones. At the end, restore the 
    # Portage configuration
    configure_live_portage
    BACKUP_LIVE="yes"

    log_msg ${LOGFILE} INFO "Search missing live packages"
    search_live_pkg
    end_msg 0

    log_msg ${LOGFILE} INFO "Live missing packages compilation"
    install_live_pkg
    end_msg 0

    restore_live_portage
    BACKUP_LIVE="no"
    clean_tmp
}

generate_live_system() {
    # Configure some parts of the live system
    # Copy the hardened components into the live system
    log_msg ${LOGFILE} INFO "Configure live system"
    configure_live_filesystem
    configure_live_fstab
    configure_live_misc
    end_msg 0


    log_msg ${LOGFILE} INFO "Copy hardened files into live system"
    copy_hardened
    end_msg 0

    clean_tmp
}

create_liveiso() {
    # Create a live ISO after configure it and 
    # copy into the ISO folder the live kernel
    log_msg ${LOGFILE} INFO "Configure grub for ISO"
    configure_iso_grub
    end_msg 0

    log_msg ${LOGFILE} INFO "Copy live kernel into live system"
    copy_liveiso
    end_msg 0

    log_msg ${LOGFILE} INFO "Create bootable ISO"
    make_iso
    end_msg 0
    clean_tmp
}

create_livecd() {
    # Burn the ISO onto a CD/DVD
    clear
    insert_disk
    burn_iso
    clean_tmp
}

create_liveusb() {
    # Search for USB media and format it before
    # configure and install live system into it
    clear
    log_msg ${LOGFILE} INFO "Create live USB"
    select_device
    format_device
    install_liveusb
    end_msg 0
    clean_tmp
}

mrproper() {
    # Remove all production files to start from scratch the next time
    # Could be usefull after changing USE flags
    # TAKE CARE WHEN YOU USE IT. YOU SHOULD RECOMPILE EVERYTHING
    # NEXT TIME
    dialog --backtitle "AIRBUS HARDENED FACTORY V0.1" --title "Remove temporary files" \
        --yesno "Do you really want to remove production and temporary files ?" ${DIALOG_DIM}
    if [ ${?} -eq 0 ]
    then 
        rm -rf ${FCT_DIR}/prod ${FCT_DIR}/tmp ${FCT_DIR}/cache ${FCT_DIR}/log
    fi
}

menu

###################
# DEBUG FUNCTIONS #
###################
print_factory_var() {
    echo "Arch ${ARCH}"
    echo "Factory path ${FCT_DIR}"
    echo "Conf path ${CFG_DIR}"
    echo "Kernel path ${KER_DIR}"
    echo "Tmp path ${TMP_DIR}"
    echo "Back up path ${BKP_DIR}"
    echo "Portage back up path ${POR_BKP}"
    echo "Package path ${PKG_DIR}"
}

interrupt() {
    read -p "Do you want to continue ? (yes/no)" -n 4 ANSWER
    case "${ANSWER}" in
        y|Y|yes|YES|YeS|yEs|Yes|yeS)
            return 0
            ;;
        *)
            exit 0
            ;;
    esac    
}

#print_factory_var
#print_hard_var
#print_live_var
